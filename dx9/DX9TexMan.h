# pragma once

# ifndef CSTB_DX9_TEXMAN_H
# define CSTB_DX9_TEXMAN_H


# include <d3dx9.h>
# include <string>
# include <map>




namespace DX9
{


// A simple texture manager
class TextureManager
{
public:

	struct TextureInfo
	{
		D3DXIMAGE_INFO		d3dxinfo;
		LPDIRECT3DTEXTURE9	texture = 0;
	};

	TextureManager() {}

	//TextureManager(){}
	~TextureManager() { Release(); }




	LPDIRECT3DTEXTURE9 GetTexture( LPDIRECT3DDEVICE9 pDevice, const WCHAR* name, D3DXIMAGE_INFO* pInfo )
	{
		if (name == nullptr || name[0] == 0)
			return nullptr;

		D3DXIMAGE_INFO dummy_ii = {0};
		D3DXIMAGE_INFO& info = pInfo ? *pInfo : dummy_ii;

		auto it = textureMap.find( name );
		if( it == textureMap.end() )
		{
			//DebugPrintf(TEXT("TextureManager: couldn't find texture, loading from file: %s"), name);
			LPDIRECT3DTEXTURE9 pTexture = 0;
			HRESULT hr = D3DXCreateTextureFromFileEx(
				pDevice,
				name,
				D3DX_DEFAULT_NONPOW2,
				D3DX_DEFAULT_NONPOW2,
				0,
				0,
				D3DFMT_A8R8G8B8,
				D3DPOOL_MANAGED,
				D3DX_DEFAULT,
				D3DX_DEFAULT,
				0,
				&info,
				0,
				&pTexture );

			auto& ti = textureMap[name];
			ti.d3dxinfo = info;
			ti.texture = pTexture;
			return pTexture;
		}
		if (it->second.texture == nullptr)
			DebugBreak();
		info = it->second.d3dxinfo;
		return it->second.texture;
	}




	void ReleaseTexture( const WCHAR* name )
	{
		auto it = textureMap.find(name);
		if( it == textureMap.end() )
			return;
		if( it->second.texture )
			it->second.texture->Release();
		textureMap.erase( it );
	}




	// Releases all textures.
	void Release()
	{
		for( auto it : textureMap )
			if( it.second.texture )
				it.second.texture->Release();
		textureMap.clear();
	}

	//static void Reload( LPDIRECT3DDEVICE9 pDevice ); // reloads all textures.

private:
	// non-copyable
	TextureManager( const TextureManager& rhs ){}
	TextureManager& operator = (const TextureManager& rhs ) { return *this; }

protected:
	std::map< std::wstring, TextureManager::TextureInfo > textureMap;
};



}; // namespace DX9




# endif