// DirectX 9 utility stuff.
// -Adel Amro


# ifndef CSTB_DX9UTIL_H
# define CSTB_DX9UTIL_H


# include <d3dx9.h>
# include <assert.h> // TODO: Move this dependancy to cpp.
# include <CSTB/Win32/Win32Util.h>



namespace DX9
{



inline void SetTextureColorStage( LPDIRECT3DDEVICE9 pD3DDevice, UINT iStage,
									DWORD arg1, DWORD op, DWORD arg2 )
{
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_COLOROP, op );
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_COLORARG1, arg1 );
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_COLORARG2, arg2 );
}

inline void SetTextureAlphaStage( LPDIRECT3DDEVICE9 pD3DDevice, UINT iStage,
								 DWORD arg1, DWORD op, DWORD arg2 )
{
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_ALPHAOP, op );
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_ALPHAARG1, arg1 );
	pD3DDevice->SetTextureStageState( (DWORD)iStage, D3DTSS_ALPHAARG2, arg2 );
}




inline D3DVERTEXELEMENT9 MakeVE( WORD stream, WORD offset, BYTE type, BYTE method, BYTE usage, BYTE usageIndex )
{
	D3DVERTEXELEMENT9 ve;
	ve.Stream = stream;
	ve.Offset = offset;
	ve.Type = type;
	ve.Method = method;
	ve.Usage = usage;
	ve.UsageIndex = usageIndex;
	return ve;
}
inline D3DVERTEXELEMENT9 MakeEndVE() { D3DVERTEXELEMENT9 ve = D3DDECL_END(); return ve; }





/// Dynamically load Direct3D9.
BOOL CreateD3DInterface( LPDIRECT3D9* ppD3D, HINSTANCE* hLib );


inline BYTE D3DCOLOR_GetRValue( D3DCOLOR c ) { return (BYTE)((c>>16)&0xFF); }
inline BYTE D3DCOLOR_GetGValue( D3DCOLOR c ) { return (BYTE)((c>>8)&0xFF); }
inline BYTE D3DCOLOR_GetBValue( D3DCOLOR c ) { return (BYTE)((c)&0xFF); }
inline BYTE D3DCOLOR_GetAValue( D3DCOLOR c ) { return (BYTE)((c>>24)&0xFF); }

inline D3DCOLOR COLORREFToD3DCOLOR( COLORREF c, BYTE alpha = 255 )
{ return D3DCOLOR_ARGB( alpha, GetRValue(c), GetGValue(c), GetBValue(c) ); }

inline COLORREF D3DCOLORToCOLORREF( D3DCOLOR c )
{ return RGB( D3DCOLOR_GetRValue(c), D3DCOLOR_GetGValue(c), D3DCOLOR_GetBValue(c) ); }


// The following six functions are copied from the Microsoft DirectX SDK.
UINT	ColorChannelBits( D3DFORMAT fmt );
UINT	AlphaChannelBits( D3DFORMAT fmt );
UINT	DepthBits( D3DFORMAT fmt );
UINT	StencilBits( D3DFORMAT fmt );
const TCHAR*	D3DFormatToString( D3DFORMAT format, bool bWithPrefix = TRUE );
const TCHAR*	D3DPresentIntervalToString( UINT pi, bool bWithPrefix = TRUE );
// TODO: More to-string functions:


const TCHAR*	D3DXParameterTypeToString( D3DXPARAMETER_TYPE type, bool bWithPrefix = TRUE );
const TCHAR*	D3DXParameterClassToString( D3DXPARAMETER_CLASS pc, bool bWithPrefix = TRUE );




/// Draws axes lines at the origin of the world in the colors commonly used in 3D packages
/// like Maya and 3d max (red for x, green for y, blue for z).
VOID DrawAxii( LPDIRECT3DDEVICE9 pDevice, FLOAT length = 1.f, const D3DXMATRIX* pTransform = NULL );



/// Draw a non-transformed quad. Use this to have the vertex shader executed.
VOID DrawQuad( LPDIRECT3DDEVICE9 pDevice, FLOAT x, FLOAT y, FLOAT z, FLOAT width, FLOAT height,
			  D3DCOLOR c1 = 0xFFFFFFFF, D3DCOLOR c2 = 0xFFFFFFFF,
			  D3DCOLOR c3 = 0xFFFFFFFF, D3DCOLOR c4 = 0xFFFFFFFF );


/// Integer-parametered variation of DrawQuad() above. It will subtract 0.5 from x and y (see D3D rasterization rules).
inline VOID DrawQuadInt( LPDIRECT3DDEVICE9 pDevice, INT x, INT y, FLOAT z, INT width, INT height,
			  D3DCOLOR c1 = 0xFFFFFFFF, D3DCOLOR c2 = 0xFFFFFFFF,
			  D3DCOLOR c3 = 0xFFFFFFFF, D3DCOLOR c4 = 0xFFFFFFFF )
{
	return DrawQuad( pDevice, (FLOAT)x - 0.5f, (FLOAT)y - 0.5f, z, (FLOAT)width, (FLOAT)height,
		c1, c2, c3, c4 );
}


/// Draws a hollow rectangle with 1-pixel border. Coordinates are in pixels.
HRESULT DrawTransformedRectangle( LPDIRECT3DDEVICE9 pDevice, const RECT& r, D3DCOLOR c );

HRESULT DrawRectangle( LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR2 v1, D3DXVECTOR2 v2, D3DXVECTOR2 v3, D3DXVECTOR2 v4, D3DCOLOR c );
inline HRESULT DrawRectangle( LPDIRECT3DDEVICE9 pDevice, float x, float y, float width, float height, D3DCOLOR c )
{
	D3DXVECTOR2 v[4];
	v[0] = D3DXVECTOR2( x, y );
	v[1] = D3DXVECTOR2( x + width, y );
	v[2] = D3DXVECTOR2( x + width, y + height );
	v[3] = D3DXVECTOR2( x, y + height );
	return DrawRectangle( pDevice, v[0], v[1], v[2], v[3], c );
}



/// Draws a transformed quad - use this if you don't need the vertex shader to be executed.
HRESULT DrawTransformedQuad( LPDIRECT3DDEVICE9 pDevice,
						 FLOAT x, FLOAT y, FLOAT z,
						 FLOAT width, FLOAT height,
						 D3DXVECTOR2 uvTopLeft, D3DXVECTOR2 uvTopRight,
						 D3DXVECTOR2 uvBottomLeft, D3DXVECTOR2 uvBottomRight,
						 D3DCOLOR c1, D3DCOLOR c2, D3DCOLOR c3, D3DCOLOR c4 );


inline HRESULT DrawTransformedQuad( LPDIRECT3DDEVICE9 pDevice,
						 FLOAT x, FLOAT y, FLOAT z,
						 FLOAT width, FLOAT height,
						 D3DCOLOR c1 = 0xFFFFFFFF,
						 D3DCOLOR c2 = 0xFFFFFFFF,
						 D3DCOLOR c3 = 0xFFFFFFFF,
						 D3DCOLOR c4 = 0xFFFFFFFF )
{
	return DrawTransformedQuad( pDevice, x, y, z, width, height,
		D3DXVECTOR2(0,0), D3DXVECTOR2(1,0), D3DXVECTOR2(0,1), D3DXVECTOR2(1,1),
		c1, c2, c3, c4 );
}


inline HRESULT DrawTransformedQuad( LPDIRECT3DDEVICE9 pDevice,
								INT x, INT y, INT width, INT height, D3DCOLOR c = 0xFFFFFFFF )
{
	return DrawTransformedQuad( pDevice, x - 0.5f, y - 0.5f, 0.f, (FLOAT)width, (FLOAT)height,
		c, c, c, c );
}

inline HRESULT DrawTransformedQuad( LPDIRECT3DDEVICE9 pDevice,
								   const RECT& rect, D3DCOLOR color = 0xFFFFFFFF )
{
	return DrawTransformedQuad( pDevice, rect.left, rect.top,
		rect.right - rect.left, rect.bottom - rect.top,
		color );
}


/// Draws a fullscreen quad. Gets size from the back buffer of the device.
VOID DrawFullscreenQuad( LPDIRECT3DDEVICE9 pDevice, FLOAT z );


HRESULT SetRenderTarget( LPDIRECT3DDEVICE9 pDevice, UINT rtIndex, LPDIRECT3DTEXTURE9 pRTTexture );


inline VOID AlphaBlendEnable( LPDIRECT3DDEVICE9 pD3DDevice, BOOL bEnable )
{
	if( bEnable )
	{
		pD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		pD3DDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
		pD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	}
	else
	{
		// Restore default alpha blending state.
		pD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
		pD3DDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_ONE );
		pD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ZERO );
	}
}





/// Wrapper base class for working with dynamic vertex and index buffers.
/// TODO: Move implementation to cpp.
class CDynamicD3DBuffer
{
protected:
	UINT				m_uSize;
	UINT				m_uOffset;
	LPDIRECT3DDEVICE9	m_pD3DDevice;
	BOOL				m_bLocked;

public:
	CDynamicD3DBuffer()
	{
		m_uSize = 0;
		m_uOffset = 0;
		m_pD3DDevice = NULL;
		m_bLocked = FALSE;
	}

	virtual ~CDynamicD3DBuffer() { Release(); }


	HRESULT Lock( UINT elementCount, UINT elementSize, UINT& offset, LPVOID* ppData )
	{
		assert( !m_bLocked );
		assert( elementSize > 0 );
		m_uOffset = Align( m_uOffset, elementSize );
		UINT size = elementCount * elementSize;
		DWORD lockFlags = D3DLOCK_NOOVERWRITE;
		if( m_uOffset + size > m_uSize )
		{
			if( size > m_uSize )
			{
				Recreate( MAX( size, m_uSize + m_uSize / 2 ) );
				//lockFlags = 0; // TODO: Why?
			}
			else
			{
				m_uOffset = 0;
				lockFlags = D3DLOCK_DISCARD;
			}
		}

		HRESULT hr = InternalLock( m_uOffset, size, lockFlags, ppData );
		if( FAILED( hr ) )
			return hr;
		m_bLocked = TRUE;
		offset = m_uOffset / elementSize;
		m_uOffset += size;
		return hr;
	}

	VOID Unlock()
	{
		assert( m_bLocked );
		InternalUnlock();
		m_bLocked = FALSE;
	}
	
	BOOL IsLocked() const { return m_bLocked; }


	virtual VOID Release() = 0
	{
		assert( !m_bLocked );
		m_uSize = 0;
		m_uOffset = 0;
		m_bLocked = FALSE;
	}

protected:
	virtual HRESULT Create( LPDIRECT3DDEVICE9 pD3DDevice, UINT size ) = 0
	{
		Release();
		m_pD3DDevice = pD3DDevice;
		m_uSize = size;
		return S_OK;
	}

	virtual HRESULT	InternalLock( UINT offset, UINT size, DWORD lockFlags, LPVOID* ppData ) = 0;	
	virtual HRESULT	InternalUnlock() = 0;


private:
	HRESULT Recreate( UINT size )
	{
		Release();
		return Create( m_pD3DDevice, size );
	}
};




/// Wrapper class for working with dynamic vertex buffers.
class CDynamicVB : public CDynamicD3DBuffer
{
protected:
	LPDIRECT3DVERTEXBUFFER9 m_pVB;

public:
	CDynamicVB() { m_pVB = NULL; }
	//~CDynamicVB() { Release(); }
	
	LPDIRECT3DVERTEXBUFFER9 GetVB() const { return m_pVB; }
	//operator LPDIRECT3DVERTEXBUFFER9 () const { return GetVB(); } // Unnecessary confusion.

	virtual VOID Release()
	{
		CDynamicD3DBuffer::Release();
		SAFE_RELEASE( m_pVB );
	}

	virtual HRESULT Create( LPDIRECT3DDEVICE9 pD3DDevice, UINT size )
	{
		CDynamicD3DBuffer::Create( pD3DDevice, size );
		return pD3DDevice->CreateVertexBuffer( size, D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &m_pVB, NULL );
	}


	virtual HRESULT InternalLock( UINT offset, UINT size, DWORD lockFlags, LPVOID* ppData )
	{ return m_pVB->Lock( offset, size, ppData, lockFlags ); }


	virtual HRESULT InternalUnlock() { return m_pVB->Unlock(); }
};





/// Wrapper class for working with dynamic index buffers.
class CDynamicIB : public CDynamicD3DBuffer
{
protected:
	LPDIRECT3DINDEXBUFFER9 m_pIB;
	D3DFORMAT m_Format;


	HRESULT Create( LPDIRECT3DDEVICE9 pD3DDevice, UINT size )
	{ return Create( pD3DDevice, size, m_Format ); }

public:
	CDynamicIB() { m_pIB = NULL; }
	//~CDynamicIB() { Release(); }
	
	LPDIRECT3DINDEXBUFFER9 GetIB() const { return m_pIB; }

	virtual VOID Release()
	{
		CDynamicD3DBuffer::Release();
		SAFE_RELEASE( m_pIB );
	}

	virtual HRESULT Create( LPDIRECT3DDEVICE9 pD3DDevice, UINT size, D3DFORMAT format )
	{
		CDynamicD3DBuffer::Create( pD3DDevice, size );
		m_Format = format;
		return pD3DDevice->CreateIndexBuffer( size, D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, format, D3DPOOL_DEFAULT, &m_pIB, NULL );
	}


	virtual HRESULT InternalLock( UINT offset, UINT size, DWORD lockFlags, LPVOID* ppData )
	{ return m_pIB->Lock( offset, size, ppData, lockFlags ); }


	virtual HRESULT InternalUnlock() { return m_pIB->Unlock(); }
};


}; // namespace DX9



# endif // include guard