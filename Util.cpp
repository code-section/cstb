# include "Util.h"
# include <math.h>
# include <string.h>
# include <stdlib.h>




void DistanceFromLine( float cx, float cy, float ax, float ay,
	float bx, float by, float& distanceSegment,
	float& distanceLine )
{
	float r_numerator = (cx-ax)*(bx-ax) + (cy-ay)*(by-ay);
	float r_denomenator = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
	float r = r_numerator / r_denomenator;

	float s =  ((ay-cy)*(bx-ax)-(ax-cx)*(by-ay) ) / r_denomenator;

	distanceLine = (float)fabs(s)*sqrtf(r_denomenator);


	float px = ax + r*(bx-ax);
	float py = ay + r*(by-ay);


	// (xx,yy) is the point on the lineSegment closest to (cx,cy)

	float xx = px;
	float yy = py;

	if ( (r >= 0) && (r <= 1) )
		distanceSegment = distanceLine;
	else
	{

		float dist1 = (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
		float dist2 = (cx-bx)*(cx-bx) + (cy-by)*(cy-by);
		if (dist1 < dist2)
		{
			xx = ax;
			yy = ay;
			distanceSegment = sqrtf(dist1);
		}
		else
		{
			xx = bx;
			yy = by;
			distanceSegment = sqrtf(dist2);
		}
	}
}




bool StringStartsWith( const char* sText, int textLen, const char* sPrefix, int prefixLen )
{
	// loop until either of the strings run out, or we find a mismatch.
	for( const char* s = sText, *sp = sPrefix;
	1;
		s++, sp++ )
	{
		if( *sp == 0 || (prefixLen >= 0 && sp == sPrefix + prefixLen) )
			return true; // sPrefix has run out without finding mismatches. It's a prefix.
		if( *s == 0 || (textLen >= 0 && s >= sText + textLen) )
			return false; // sText has run out before matching sPrefix.
		if( *sp != *s )
			return false;
	}
	return false;
}



bool StringStartsWith( const wchar_t* sText, int textLen, const wchar_t* sPrefix, int prefixLen )
{
	// loop until either of the strings run out, or we find a mismatch.
	for( const wchar_t* s = sText, *sp = sPrefix;
	1;
		s++, sp++ )
	{
		if( *sp == 0 || (prefixLen >= 0 && sp == sPrefix + prefixLen) )
			return true; // sPrefix has run out without finding mismatches. It's a prefix.
		if( *s == 0 || (textLen >= 0 && s >= sText + textLen) )
			return false; // sText has run out before matching sPrefix.
		if( *sp != *s )
			return false;
	}
	return false;
}



int FindSubstring( const char* sText, int textLen, const char* sFind, int findLen )
{
	if( !sText && sFind && sFind[0] != 0 )
		return -1;
	for( int i = 0; sText[i] != 0 && !(textLen >= 0 && i >= textLen); i++ )
		if( StringStartsWith( &sText[i], textLen >= 0 ? (textLen-i) : -1, sFind, findLen ) )
			return i;
	return -1;
}



int FindSubstring( const wchar_t* sText, int textLen, const wchar_t* sFind, int findLen )
{
	if( !sText && sFind && sFind[0] != 0 )
		return -1;
	for( int i = 0; sText[i] != 0 && !(textLen >= 0 && i >= textLen); i++ )
		if( StringStartsWith( &sText[i], textLen >= 0 ? (textLen-i) : -1, sFind, findLen ) )
			return i;
	return -1;
}


/// Copies one string to another. The function will 0-terminate the destination buffer,
/// unless its size is 0. It returns the number of characters copied to the destination
/// buffer, not including the terminating 0.
/// The length of the source string can be -1, but the function will stop copying when it
/// encounters a 0 in the source string.
int StrncpyEx( char* dest, int destSize, const char* src, int srcLen )
{
	if( !dest || destSize <= 0 || !src )
		return 0;

	int i = 0;
	for( ; i<destSize-1; i++ )
	{
		if( (srcLen >= 0 && i >= srcLen) || *src == 0 )
			break;
		*dest++ = *src++;
	}
	*dest = 0;
	return i;
}



int ReplaceString( char* dest, int destSize, const char* src, const char* find, const char* replace,
	int maxOccurrances )
{
	if( destSize <= 0 )
		return 0;

	if( maxOccurrances == 0 )
		return StrncpyEx( dest, destSize, src, -1 );

	int findLen = strlen( find );
	int replaceLen = strlen( replace );
	int numWritten = 0;
	int occurrances = 0;
	do
	{
		int pos = FindSubstring( src, -1, find, findLen );
		int n = StrncpyEx( dest + numWritten, destSize - numWritten, src, pos );
		numWritten += n;
		src += n;

		if( pos < 0 )
			return numWritten;

		n = StrncpyEx( dest + numWritten, destSize - numWritten, replace, replaceLen );
		numWritten += n;
		src += findLen;

		if( ++occurrances == maxOccurrances )
			return numWritten;

		if( numWritten >= destSize-1 )
			return numWritten;
	} while(1);
	return 0;
}