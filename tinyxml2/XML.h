/**
Wrapper for tinyxml2.
Copyright Adel Amro - http://code-section.com
*/

# ifndef CSTB_XML_H
# define CSTB_XML_H


# include <windows.h>


namespace tinyxml2
{
class XMLDocument;
class XMLElement;
};



namespace XML
{


/// This class is a handle to an XML element in an XML document.
/// An element can have any number of attributes identified by name.
/// Elements are created and owned by an XML document (see the XML class below).
class HELEMENT
{
private:
	tinyxml2::XMLElement* m_pElem;
	friend class Document;
	HELEMENT( tinyxml2::XMLElement* pElem );

public:
	HELEMENT() { m_pElem = NULL; }
	void SetAttribute( const char* name, int value );
	void SetAttribute( const char* name, bool value ) { return SetAttribute( name, (int)value ); }
	void SetAttribute( const char* name, double value );
	void SetAttribute( const char* name, float value ) { return SetAttribute( name, (double)value ); }
	void SetAttribute( const char* name, const char* value );
	void SetAttribute( const char* name, const WCHAR* value );
	void SetValue( const char* value );
	void SetValue( const WCHAR* value );

	bool		GetAttribute( const char* name, int& value ) const;
	bool		GetAttribute( const char* name, double& value ) const;
	bool		GetAttribute( const char* name, float& value ) const;
	bool		GetAttribute( const char* name, bool& value ) const;
	bool		GetAttribute( const char* name, WCHAR* s, INT bufferLen );
	const char* GetAttribute( const char* name ) const;
	const char* GetValue() const;
	const char* GetText() const; ///< Returns the text of the XML element. For example, "stuff" in <elem>stuff</elem>

	void GetSetAttribute( const char* name, bool get, int& value )
	{ if( get ) GetAttribute( name, value ); else SetAttribute( name, value ); }

	void GetSetAttribute( const char* name, bool get, bool& value )
	{ if( get ) GetAttribute( name, value ); else SetAttribute( name, value ); }

	void GetSetAttribute( const char* name, bool get, double& value )
	{ if( get ) GetAttribute( name, value ); else SetAttribute( name, value ); }

	void GetSetAttribute( const char* name, bool get, float& value )
	{ if( get ) GetAttribute( name, value ); else SetAttribute( name, value ); }

	void GetSetAttribute( const char* name, bool get, LPWSTR ws, int ccBuff )
	{ if( get ) GetAttribute( name, ws, ccBuff ); else SetAttribute( name, ws ); }

	int	GetIntAttr( const char* name, int defaultValue = 0, bool* succeeded = 0 )
	{ int ret = defaultValue; bool s = GetAttribute( name, ret ); if( succeeded ) *succeeded = s; return ret; }

	double GetDoubleAttr( const char* name, double defaultValue = 0.0, bool* succeeded = 0 )
	{ double ret = defaultValue; bool s = GetAttribute( name, ret ); if( succeeded ) *succeeded = s; return ret; }

	bool GetBoolAttr( const char* name, bool defaultValue = false, bool* succeeded = 0 )
	{ bool ret = defaultValue; bool s = GetAttribute( name, ret ); if( succeeded ) *succeeded = s; return ret; }

	//const char* AttribString( const char* name, const char* defaultValue = NULL, bool* succeeded = 0 )
	//{ const char* ret = defaultValue; bool s = GetAttribute( name, ret ); if( succeeded ) *succeeded = s; return ret; }


	void DeleteAttribute( const char* name );

	operator bool () const { return m_pElem != NULL; }

	unsigned CountChildren() const;
	HELEMENT FirstChildElement( const char* name = NULL ) const;
	HELEMENT CreateChildElement( const char* name );
	HELEMENT FindOrCreateChildElement( const char* name )
	{
		HELEMENT hChild = FirstChildElement( name );
		if( hChild ) return hChild;
		return CreateChildElement( name );
	}
	/// Equivalent to FindOrCreateChildElement()
	HELEMENT GetChildElement( const char* name ) { return FindOrCreateChildElement( name ); }

	HELEMENT operator [] ( const char* name ) { return FindOrCreateChildElement(name); }
	HELEMENT operator [] ( const char* name ) const { return FirstChildElement(name); }

	tinyxml2::XMLElement* GetXmlElement() const { return m_pElem; }
};




/// An XML document.
class Document
{
public:
	Document();
	~Document();

	/// Creates an element
	HELEMENT CreateElement( const char* name, HELEMENT hParent = NULL );

	/// Finds an element if it exists. Returns NULL if the element doesn't exist.
	HELEMENT FindElement( const char* name, HELEMENT hParent = NULL );

	/// Delete an element. The handle becomes invalid.
	void DeleteElement( HELEMENT hElement );

	/// Creates a new element or returns an already-existing element.
	HELEMENT FindOrCreateElement( const char* name, HELEMENT hParent = NULL )
	{
		HELEMENT hRet = FindElement( name, hParent );
		if( hRet ) return hRet;
		return CreateElement( name, hParent );
	}

	/// Equivalent to FindOrCreateElement().
	HELEMENT GetElement( const char* name, HELEMENT hParent = NULL )
	{ return FindOrCreateElement( name, hParent ); }


	/// @{
	/// Element navigation methods. Most are static.
	HELEMENT		GetFirst(); /// Returns the first element.
	HELEMENT		GetLast(); /// Returns the last element in the group.
	static HELEMENT	GetParent( HELEMENT e ); /// Returns the parent element of the specified element.
	static HELEMENT	GetFirstChild( HELEMENT e ); /// Returns the first child element of an element, if there is any.
	static HELEMENT	NextSibling( HELEMENT e ); /// Returns the next sibling of the specified element.
	static HELEMENT	PrevSibling( HELEMENT e ); /// Returns the previous sibling of the specified element.
	/// @}

	//void	Clear();
	bool	Parse( const char* sXML ); ///< NOTE: This invalidates all existing element handles.
	bool	Load( LPCSTR sFileName ); ///< NOTE: This invalidates all existing element handles.
	bool	Load( LPCWSTR sFileName );
	bool	Save( LPCSTR sFileName );
	bool	Save( LPCWSTR sFileName );
	int		ErrorID() const;

	/// Works only if the file name has already been established by a previous call to Load(sFileName) or Save(sFileName).
	bool	Save();
	//LPCSTR	GetFileName() const;

	/// Retunrs the underlying tinyxml 2 document.
	tinyxml2::XMLDocument* GetXmlDocument() const { return pDoc; }

	/// Shortcut for GetElement()
	HELEMENT operator [] ( const char* name ) { return GetElement(name); }

private:
	tinyxml2::XMLDocument* pDoc;

	// not copyable.
	Document& operator = (const Document&);
	Document( const Document&);
};


}; // namespace XML

# endif