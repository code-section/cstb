
# include "SciterWindow.h"
# include <CSTB/Win32/Win32Util.h>
# include <sciter-x-behavior.h>
# include <strsafe.h>
# include <map>
# include <list>



namespace CSTB
{


const unsigned IDM_RELOAD = 38383;




BOOL SciterWindow::Create( DWORD dwStyleEx, LPCTSTR sTitle, DWORD dwStyle,
		int x, int y, int width, int height, HWND hParentWnd,
		HMENU hMenu, HINSTANCE hInstance, void* lParam )
{
	if( !Window::Create( dwStyleEx, CSTB_WINDOWCLASS, sTitle, dwStyle, x, y, width, height,
		hParentWnd, hMenu, hInstance, lParam ) )
		return FALSE;

	SciterSetCallback( hWnd, SciterWindow::NotifyHandler, (LPVOID)this );

	sciter::attach_dom_event_handler( hWnd, this );

	// Set a global variable with sciter so we can distinguish in tiscript between
	// content loaded by curver and content loaded elsewhere (e.g. sciter.exe).
	// This is done by using media variables.

	/*
	sciter::value globals;
	globals["CURVER"] = true;

#if defined( DEBUG ) || defined( _DEBUG ) || defined( __DEBUG )
	globals["DEBUG"] = true;
	SciterSetOption(hWnd, SCITER_SET_DEBUG_MODE, TRUE);
#else
	globals["RELEASE"] = true;
# endif

	SciterSetMediaVars(hWnd, &globals);
	*/

	// In CSS, this can be used to create app-specific css rules:
	// @media CURVER {
	// div.red{ color: red; } <<-- app-specific
	// }

	// In tiscript, it can be used as:
	// const isCurver = view.mediaVars()["CURVER"];

	// Add a "Reload" menu entry to the system menu.
	HMENU hSysMenu = GetSystemMenu( hWnd, FALSE );
	AppendMenu( hSysMenu, MF_STRING, IDM_RELOAD, TEXT("Reload") );

	return TRUE;
}




BOOL SciterWindow::LoadFile( LPCTSTR sPath, BOOL bMakeAbsPath )
{
	if( !hWnd || !sPath )
		return FALSE;


	TCHAR sAbsPath[ MAX_PATH ];
	TCHAR sFullPath[ MAX_PATH ];
	sAbsPath[0] = 0;
	sFullPath[0] = 0;

	if( bMakeAbsPath )
	{
		LPTSTR sFilePart = NULL;
		DWORD len = GetFullPathName( sPath, MAX_PATH, sAbsPath, &sFilePart );
		::StringCchPrintf( sFullPath, MAX_PATH, TEXT("file://%s"), sAbsPath );
		sPath = sFullPath;
	}

	m_sFileName = sPath;

	return SciterLoadFile( hWnd, sPath );
}



LRESULT SciterWindow::MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
{
	BOOL bHandled = FALSE;
	LRESULT ret = 0;
	ret = SciterProcND( hWnd, Msg, wParam, lParam, &bHandled );
	if( bHandled )
		return ret;

	if( (Msg == WM_SYSCOMMAND && wParam == IDM_RELOAD) ||
		(Msg == WM_KEYDOWN && wParam == VK_F5) )
	{
		if( !Reload() )
			OutputDebugString( TEXT("Failed to reload\n") );
	}

	/*
	// TODO: Consider removing all script callbacks associated with this window.
	if( Msg == WM_DESTROY )
	{
	}
	*/

	return Window::MsgProc( Msg, wParam, lParam );
}




// Loop through the global list of behavior factories and find the
// behavior matching the name, and attach it to the element.
LRESULT SciterWindow::OnAttachBehavior( LPSCN_ATTACH_BEHAVIOR pn )
{
	sciter::event_handler *pb = sciter::behavior_factory::create(pn->behaviorName, pn->element);
	if(pb)
	{
		pn->elementTag  = pb;
		pn->elementProc = sciter::event_handler::element_proc;
		return TRUE;
	}
	return FALSE;
}

}; // namespace CSTB