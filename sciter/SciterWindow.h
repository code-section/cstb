# ifndef CSTB_SCITER_WINDOW_H
# define CSTB_SCITER_WINDOW_H

# include <CSTB\Win32\Window.h>
# include <tchar.h>
# include <sciter-x.h>
# include "SciterUtil.h"




namespace CSTB
{

class SciterWindow : public Window, public sciter::event_handler
{
public:
	/// Creates the window and sets up the callback which calls the OnSciterNotify() method.
	BOOL Create( DWORD dwStyleEx, LPCTSTR sTitle, DWORD dwStyle,
		int x, int y, int width, int height, HWND hParentWnd,
		HMENU hMenu, HINSTANCE hInstance, void* lParam );

	virtual BOOL LoadFile( LPCTSTR sPath, BOOL bMakeAbsPath = TRUE );
	virtual BOOL Reload() { return m_sFileName.empty() ? FALSE : LoadFile( m_sFileName.c_str(), FALSE ); }

	/// Passes messages to SciterProcND() then to AWindow::DefWindowProc().
	virtual LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam ) override;

	
	sciter::NativeFunctionMap* pFunctionMap = nullptr;

	virtual bool on_script_call(HELEMENT he, LPCSTR name, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval) override
	{
		if (pFunctionMap)
			return pFunctionMap->CallFunction(he, name, argc, argv, retval);
		return false;
	}



	virtual LRESULT OnSciterNotify(LPSCITER_CALLBACK_NOTIFICATION pn)
	{
		if (pn->code == SC_ATTACH_BEHAVIOR)
			return OnAttachBehavior((LPSCN_ATTACH_BEHAVIOR)pn);
		return 0;
	}

	virtual LRESULT OnAttachBehavior( LPSCN_ATTACH_BEHAVIOR pn );

	/*/// Call script function that is defined in tiscript code loaded in this window.
	BOOL Call( LPCSTR functionName, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval )
	{ return SciterCall( hWnd, functionName, argc, argv, &retval ); }*/


	//static sciter::NativeFunctionMap	nativeFunctions;
	//static sciter::CallbackMap		eventCallbacks;

protected:
	static UINT CALLBACK NotifyHandler( LPSCITER_CALLBACK_NOTIFICATION pNotification, LPVOID lParam )
	{
		SciterWindow* pWnd = (SciterWindow*)lParam;
		if( pWnd ) return pWnd->OnSciterNotify( pNotification );
		return 0;
	}

	sciter::string m_sFileName;
};


}; // namespace CSTB



# endif