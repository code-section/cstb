# ifndef CSTB_SCITER_UTIL_H
# define CSTB_SCITER_UTIL_H


# include <sciter-x.h>
# include <map>
# include <list>


namespace sciter
{
	/// Utility function to update native variables from sciter objects.
	///	Usage example: int n = default_int_value; sciter::GetValue( val["my_integer"], n );
	template<typename T>
	inline T GetValue(const value_key_a vk, T& t)
	{ t = value(vk).get(t); return t; }

	/// Template customization to allow us to use GetValue() with float references since sciter uses doubles.
	inline float GetValue(const sciter::value_key_a vk, float& t)
	{ double d = (double)t; return t = (float)GetValue(vk, d); }


	/// Exchange data between script and native variables. Usage:
	/// ExchangeValue( b_from_native, val["key"], refNativeVar );
	template<typename T>
	inline void ExchangeValue(bool toNative, value_key_a&& vk, T& t)
	{ if (toNative) GetValue(vk, t); else vk = t; }





	// These allow us to get a function pointer from a lambda expression
	namespace f_util
	{
		template <typename Function>
		struct function_traits : public function_traits<decltype(&Function::operator())>
		{};

		template <typename ClassType, typename ReturnType, typename... Args>
		struct function_traits<ReturnType(ClassType::*)(Args...) const>
		{
			typedef ReturnType(*pointer)(Args...);
			typedef std::function<ReturnType(Args...)> function;
		};

		template <typename Function>
		typename function_traits<Function>::pointer
			to_function_pointer(Function& lambda)
		{
			return static_cast<typename function_traits<Function>::pointer>(lambda);
		}
	}




	template <typename Function>
	sciter::value vlambda(Function lambda)
	{
		// If you get "illegal use of type 'void' error", make sure your lambda expression returns a sciter::value.
		return sciter::vfunc(f_util::to_function_pointer(lambda));
	}




	// The callback map is used to link script functions to native events by name.
	struct CallbackMap
	{
		struct ScriptCallback {
			HELEMENT he;
			sciter::value vf;
			bool operator == (const ScriptCallback& rhs) const { return he == rhs.he && vf == rhs.vf; }
		};

		std::map< std::string, std::list< ScriptCallback > > callbackMap;

		void AddCallback(const char* eventName, HELEMENT he, sciter::value vf )
		{
			ScriptCallback cb = { he, vf };
			if( eventName && eventName[0] != 0 && !vf.is_undefined() && !vf.is_null() )
				callbackMap[eventName].push_back(cb);
		}

		void RemoveCallback( const char* eventName, HELEMENT he, sciter::value vf )
		{
			ScriptCallback cb = { he, vf };
			callbackMap[eventName].remove(cb);
		}

		void RemoveCallbacks(HELEMENT he) ///< Remove all callbacks for the specified window.
		{
			auto lambda = [he](const ScriptCallback& cb)->bool { return cb.he == he; };

			for (auto& callbackList : callbackMap)
				callbackList.second.remove_if(lambda);
		}

		sciter::value FireEvent(const char* name, int argc, const sciter::value* argv)
		{
			sciter::value ret;
			auto it = callbackMap.find(name);
			if (it == callbackMap.end())
				return ret;

			for (auto& cb : it->second)
			{
				auto callRet = cb.vf.call(argc, argv);
				if (!callRet.is_undefined() && !callRet.is_null())
					ret = callRet;
			}
			return ret;
		}

		sciter::value FireEvent(const char* name)
		{ return FireEvent(name, 0, 0); }

		sciter::value FireEvent( const char* name, sciter::value param1 )
		{ return FireEvent(name, 1, &param1); }
	};


	

	struct NativeFunctionMap
	{
		typedef bool(*NFunction)(HELEMENT he, LPCSTR name, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval);
		
		std::map< std::string, NFunction > fMap;

		void AddNativeFunction(const char* name, NFunction nf)
		{ fMap[name] = nf; }

		void RemoveNativeFunction(const char* name)
		{ fMap.erase(name); }

		NFunction GetFunction(const char* name)
		{
			auto it = fMap.find(name);
			if (it == fMap.end())
				return nullptr;
			return it->second;
		}

		bool CallFunction(HELEMENT he, const char* name, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval)
		{
			auto pf = GetFunction(name);
			if (!pf)
				return false;
			return pf( he, name, argc, argv, retval);
		}
	};


	// used inside sciter's BEGIN_FUNCTION_MAP and END_FUNCTION_MAP
#define FUNCTION_V_SCRIPT_CALL(name, method) \
	if( const_chars(name) == _name )\
		return method( he, name, argc, argv, retval );
}




# endif