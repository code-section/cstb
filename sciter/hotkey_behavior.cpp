# include <CSTB\Win32\Win32Util.h>
# include <tchar.h>
# include <sciter-x.h>


namespace sciter
{



struct hotkey_behavior : public event_handler
{
	HWND hWnd;
	HELEMENT self;

	hotkey_behavior(): event_handler()
	{
		hWnd = 0;
		self = 0;
	}

	virtual void attached( HELEMENT he )
	{
		assert( hWnd == 0 );
		self = he;
		dom::element el = he;
		HWND hParent = el.get_element_hwnd( true );
		hWnd = CreateWindowEx(	0,
								HOTKEY_CLASS,
								TEXT(""),
								WS_CHILD | WS_VISIBLE,
								0, 0, 0, 0,
								hParent,
								0,
								(HINSTANCE)GetWindowLongPtr( hParent, GWLP_HINSTANCE ),
								0);

		ModifyWindowExStyle( hWnd, WS_EX_CLIENTEDGE, 0 ); // Remove the ugly client edge
		SendMessage( hWnd, WM_SETFONT, (WPARAM)GetGuiFont(), TRUE ); // Use a decent font
		el.attach_hwnd( hWnd );
	}


	virtual void detached( HELEMENT he )
	{
		if( hWnd && IsWindow( hWnd ) )
			DestroyWindow( hWnd );
		hWnd = 0;
		self = 0;
		dom::element el = he;
		el.attach_hwnd( 0 );
		delete this;
	}


	virtual bool on_script_call(HELEMENT he, LPCSTR name, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval) override
	{
		if( strcmp( name, "Set" ) == 0 && argc == 1 )
			SendMessage( hWnd, HKM_SETHOTKEY, argv[0].get(0), 0 );
		else if( strcmp( name, "Get" ) == 0 )
			retval = (int)SendMessage( hWnd, HKM_GETHOTKEY, 0, 0 );
		else if( strcmp( name, "Focus" ) == 0 )
			SetFocus( hWnd );
		else if( strcmp( name, "GetText" ) == 0 )
		{
			ACCEL a = HotkeyToAccel( hWnd );
			retval = GetAccelName( a );
		} else if( strcmp( name, "PreventModifiers" ) == 0 )
		{
			SendMessage( hWnd, HKM_SETRULES, HKCOMB_S | HKCOMB_C | HKCOMB_A | HKCOMB_SC | HKCOMB_SA | HKCOMB_CA | HKCOMB_SCA, 0 );
			retval = true;
		}
		return true;
	}


	struct factory : public behavior_factory
	{
		factory() : behavior_factory("hotkey") { }
		virtual event_handler* create( HELEMENT he ) { return new hotkey_behavior; }
	};
};



// This factory instance registers the hotkey behavior with sciter.
hotkey_behavior::factory hotkey_factory_instance;




}; // namespace sciter