
# include "ColorBoxCtrl.h"
# include <CSTB\Win32\Win32Util.h>
# include <strsafe.h>



// Structure containing the data used internally by the control.
struct ColorBoxInfo
{
	HWND hWnd;
	COLORREF color;
	COLORBOX_TEXTMODE textMode;
	HFONT hFont;

	ColorBoxInfo( HWND _hWnd )
	{
		hWnd = _hWnd;
		color = RGB(0,0,0);
		textMode = CBXT_BYTE;
		hFont = GetGuiFont();
	}
};




COLORREF g_aCustColors[16];



ColorBoxInfo*	GeColorBoxInfo( HWND hWnd ) { return (ColorBoxInfo*)GetWindowLongPtr( hWnd, 0 ); }
VOID			SeColorBoxInfo( HWND hWnd, ColorBoxInfo* pCBI ) { SetWindowLongPtr( hWnd, 0, (LONG_PTR)pCBI ); }





COLORREF ColorBox_GetColor( HWND hWnd )
{
	ColorBoxInfo* pCBI = GeColorBoxInfo( hWnd );
	return pCBI->color;
}




COLORBOX_TEXTMODE ColorBox_GetTextMode( HWND hWnd )
{ return GeColorBoxInfo( hWnd )->textMode; }





VOID ColorBox_SetTextMode( HWND hWnd, COLORBOX_TEXTMODE newMode )
{
	if( ColorBox_GetTextMode( hWnd ) == newMode )
		return;
	GeColorBoxInfo( hWnd )->textMode = newMode;
	InvalidateRect( hWnd, NULL, TRUE );
}




LPCTSTR ColorBox_GetText( HWND hWnd )
{
	COLORBOX_TEXTMODE tm = ColorBox_GetTextMode( hWnd );

	switch( tm )
	{
	case CBXT_NONE:
		return NULL;

	case CBXT_CALLBACK:
		{
			NMHDR hdr;
			hdr.code = CBXN_GETTEXT;
			hdr.hwndFrom = hWnd;
			hdr.idFrom = GetWindowLong( hWnd, GWL_ID );
			return (LPCTSTR)SendMessage( GetParent( hWnd ), WM_NOTIFY, hdr.idFrom, (LPARAM)&hdr );
		}
		return NULL;

	default:
		{
			COLORREF c = ColorBox_GetColor( hWnd );
			static TCHAR sText[ 32 ];
			sText[0] = 0;
			if( tm == CBXT_BYTE )
				StringCchPrintf( sText, ARRAY_SIZE(sText),
								TEXT("%d, %3d, %3d"),
								GetRValue(c), GetGValue(c), GetBValue(c) );
			else if( tm == CBXT_FLOAT )
				StringCchPrintf( sText, ARRAY_SIZE(sText),
								TEXT("%.2f, %6.2f, %6.2f"),
								GetRValue(c)/255.f, GetGValue(c)/255.f, GetBValue(c)/255.f );
			return sText;
		}
	}
}






// Return color to be used to draw text. Decided based on background color to ensure it's always clearly visible.
COLORREF ColorBox_GetTextColor( HWND hWnd )
{
	COLORREF c = ColorBox_GetColor( hWnd );
	INT darkness = (255 - GetRValue(c)) + (255 - GetGValue(c)) + (255 - GetBValue(c));
	INT brightness = GetRValue(c) + GetGValue(c) + GetBValue(c);
	if( darkness > brightness )
		return RGB( 255, 255, 255 );
	return RGB(0,0,0);
}




// Sets the color of the control
VOID ColorBox_SetColor( HWND hWnd, COLORREF c, BOOL bUpdate, BOOL bNotifyParent )
{
	ColorBoxInfo* pCBI = GeColorBoxInfo( hWnd );
	if( bNotifyParent )
	{
		NM_COLORBOX nmcbx;
		nmcbx.hdr.code = CBXN_COLORCHANGING;
		nmcbx.hdr.hwndFrom = hWnd;
		nmcbx.hdr.idFrom = GetWindowLong( hWnd, GWL_ID );
		nmcbx.newColor = c;
		nmcbx.oldColor = ColorBox_GetColor( hWnd );
		if( 0 != SendMessage( GetParent(hWnd), WM_NOTIFY, nmcbx.hdr.idFrom, (LPARAM)&nmcbx ) )
			return;

		ColorBox_SetColor( hWnd, c, FALSE, FALSE );
		nmcbx.hdr.code = CBXN_COLORCHANGE;
		SendMessage( GetParent(hWnd), WM_NOTIFY, nmcbx.hdr.idFrom, (LPARAM)&nmcbx );
		SendMessage( GetParent(hWnd), WM_COMMAND, MAKEWPARAM( GetWindowLong( hWnd, GWL_ID ), CBXN_COLORCHANGE ), (LPARAM)hWnd );
		return;
	}
	pCBI->color = c;
	InvalidateRect( hWnd, NULL, FALSE );
	if( bUpdate )
		UpdateWindow( hWnd );
}







VOID ColorBox_PickColor( HWND hWnd )
{
	CHOOSECOLOR cc;
	ZeroMemory( &cc, sizeof(CHOOSECOLOR) );
	cc.lStructSize = sizeof(CHOOSECOLOR);
	//cc.hwndOwner = GetWindow( hWnd, GW_OWNER );//GetParent(hWnd);
	cc.hwndOwner = hWnd;
	cc.rgbResult = ColorBox_GetColor( hWnd );
	cc.lpCustColors = g_aCustColors;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if( TRUE == ChooseColor( &cc ) )
		ColorBox_SetColor( hWnd, cc.rgbResult, TRUE, TRUE );
}




// The message processor. Almost does nothing. Just paints the control using its color.
LRESULT CALLBACK ColorBoxMsgProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	ColorBoxInfo* pCBI = GeColorBoxInfo( hWnd );
	switch( Msg )
	{
	case WM_NCCREATE:
		{
			pCBI = new ColorBoxInfo( hWnd );
			pCBI->color = 0x00000000;
			SeColorBoxInfo( hWnd, pCBI );
			// Give the control a border.
			DWORD dwStyle = GetWindowLong( hWnd, GWL_STYLE );
			SetWindowLong( hWnd, GWL_STYLE, dwStyle | WS_BORDER );
			DWORD dwExStyle = GetWindowLong( hWnd, GWL_EXSTYLE );
			SetWindowLong( hWnd, GWL_EXSTYLE, dwExStyle | WS_EX_STATICEDGE );
		}
		break;

	case WM_NCDESTROY:
		delete pCBI;
		break;

	case WM_SETFONT:
		pCBI->hFont = (HFONT)wParam;
		if( LOWORD(lParam) )
		{
			InvalidateRect( hWnd, NULL, FALSE );
			UpdateWindow( hWnd );
		}
		return 0;

	case WM_GETFONT: return (LRESULT)pCBI->hFont;

	case WM_SIZE: InvalidateRect( hWnd, NULL, TRUE ); break;

	case WM_ERASEBKGND:
		return 1;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdcPaint = BeginPaint( hWnd, &ps );
			if( !hdcPaint )
				break;

			RECT rClient;
			GetClientRect( hWnd, &rClient );

			{
				// Fill with the control's color.
				//FillRect( hdcPaint, &ps.rcPaint, pCBI->hBrush );
				COLORREF oldColor = SetBkColor( hdcPaint, pCBI->color );
				ExtTextOut( hdcPaint, 0, 0, ETO_OPAQUE, &ps.rcPaint, TEXT(""), 0, 0 ); // Faster than FillRect() it seems.
				SetBkColor( hdcPaint, oldColor );
			}

			LPCTSTR sText = ColorBox_GetText( hWnd );
			if( sText && sText[0] != 0 )
			{
				HFONT hOldFont = (HFONT)SelectObject( hdcPaint, (HFONT)SendMessage( hWnd, WM_GETFONT, 0, 0 ) );
				INT oldBkMode = SetBkMode( hdcPaint, TRANSPARENT );
				COLORREF oldTextColor = SetTextColor( hdcPaint, ColorBox_GetTextColor( hWnd ) );
				DrawText( hdcPaint, sText, -1, &rClient, DT_SINGLELINE | DT_CENTER | DT_VCENTER | DT_NOCLIP );

				SetTextColor( hdcPaint, oldTextColor );
				SetBkMode( hdcPaint, oldBkMode );
				SelectObject( hdcPaint, hOldFont );
			}

			if( GetFocus() == hWnd )
			{
				InflateRect( &rClient, -2, -2 );
				DrawFocusRect( hdcPaint, &rClient );
			}
			EndPaint( hWnd, &ps );
		}
		break;


	//case WM_MOUSEACTIVATE: SetFocus( hWnd ); break;

	case WM_KEYDOWN:
		if( wParam == VK_SPACE )
		{
			ColorBox_PickColor( hWnd );
			return 0;
		}
		break;

	case WM_KILLFOCUS:
	case WM_SETFOCUS:
		InvalidateRect( hWnd, NULL, TRUE );
		UpdateWindow( hWnd );
		break;

	case WM_GETDLGCODE: return DLGC_BUTTON;

	case BM_CLICK:
	case WM_LBUTTONDOWN:
		{
			NMHDR nmhdr;
			nmhdr.code = NM_CLICK;
			nmhdr.hwndFrom = hWnd;
			nmhdr.idFrom = GetWindowLong( hWnd, GWL_ID );
			LRESULT ret = SendMessage( GetParent( hWnd ), WM_NOTIFY, 0, (LPARAM)&nmhdr );
			if( ret == 0 )
				ColorBox_PickColor( hWnd );
			SetFocus( hWnd );
		}
		break;
	}
	return DefWindowProc( hWnd, Msg, wParam, lParam );
}




// Call this before creating any color box windows. It registers the control's window class.
BOOL ColorBox_Init( HINSTANCE hInstance )
{
	static BOOL bInited = FALSE;
	static BOOL bSucceeded = FALSE;
	if( bInited )
		return bSucceeded;
	bInited = TRUE;

	// Register the color box window class.
	WNDCLASSEX wc_colorbox;
	ZeroMemory( &wc_colorbox, sizeof(WNDCLASSEX) );

	wc_colorbox.cbSize = sizeof( WNDCLASSEX );
	wc_colorbox.lpfnWndProc = ColorBoxMsgProc;
	wc_colorbox.cbWndExtra = sizeof( ColorBoxInfo* );
	wc_colorbox.hInstance = hInstance;
	wc_colorbox.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc_colorbox.lpszClassName = WC_COLORBOX;
	if( 0 == RegisterClassEx( &wc_colorbox ) )
	{
		OutputDebugString( TEXT( "Failed to register the color box window class!" ) );
		bSucceeded = FALSE;
		return FALSE;
	}

	// Initialize the array of custom colors used with the color picker dialog.
	for( UINT i=0; i<4; i++ )	g_aCustColors[i] = RGB( 255/(i%4+1),	0,				0 );
	for( UINT i=4; i<8; i++ )	g_aCustColors[i] = RGB( 0,				255/(i%4+1),	0 );
	for( UINT i=8; i<12; i++ )	g_aCustColors[i] = RGB( 0,				0,				255/(i%4+1) );
	for( UINT i=12; i<16; i++ )	g_aCustColors[i] = RGB( 255/(i%4+1),	255/(i%4+1),	255/(i%4+1) );
	bSucceeded = TRUE;
	return TRUE;
}



// Unregister control's window class.
VOID ColorBox_Uninit( HINSTANCE hInstance )
{
	UnregisterClass( WC_COLORBOX, hInstance );
}