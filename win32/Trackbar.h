
# ifndef CSTB_TRACKBAR_H
# define CSTB_TRACKBAR_H


# include "Window.h"
# include "Win32Util.h"


namespace CSTB
{


/// Simple wrapper for the win32 trackbar control
class Trackbar : public Window
{
public:
	Trackbar() { }

	/// Creates the trackbar control. set style to 0 to use default control style.
	BOOL Create( unsigned id, DWORD style, DWORD styleEx, int x, int y,
				int width, int height, INT iMin, INT iMax, INT iSelMin, INT iSelMax,
				HWND parent, HINSTANCE hInstance )
	{
		if( style == 0 ) style = WS_CHILD | /*TBS_AUTOTICKS | */TBS_ENABLESELRANGE;
		if( width < 0 ) width = 200;
		if( height < 0 ) height = 30;

		if( FALSE == Window::Create( styleEx, TRACKBAR_CLASS, NULL, style, x, y, width, height,
			parent, (HMENU)id, hInstance, NULL ) )
			return FALSE;

		HFONT guiFont = (HFONT)GetStockObject( DEFAULT_GUI_FONT );
		SendMessage( hWnd, WM_SETFONT, (WPARAM)guiFont, (LPARAM)TRUE );

		SetRange( iMin, iMax, FALSE );
		SetSel( iSelMin, iSelMax, FALSE );

		ShowWindow( hWnd, SW_SHOW );
		UpdateWindow( hWnd );
		return true;
	}

	VOID SetRange( INT iMin, INT iMax, BOOL bRedraw )
	{ SendMessage( hWnd, TBM_SETRANGE, (WPARAM)bRedraw, (LPARAM)MAKELONG(iMin, iMax) ); }

	VOID SetPageSize( INT iPageSize )
	{ SendMessage( hWnd, TBM_SETPAGESIZE, 0, (LPARAM)iPageSize ); }

	VOID SetSel( INT iSelMin, INT iSelMax, BOOL bRedraw )
	{ SendMessage( hWnd, TBM_SETSEL, (WPARAM)bRedraw, (LPARAM)MAKELONG( iSelMin, iSelMax) ); }

	VOID SetPos( INT iPos, BOOL bRedraw )
	{ SendMessage( hWnd, TBM_SETPOS, (WPARAM)bRedraw, (LPARAM)iPos ); }

	INT GetPos() { return (INT)SendMessage( hWnd, TBM_GETPOS, 0, 0 ); }

	VOID EnableSelRange( BOOL bEnable )
	{ ModifyWindowStyle( hWnd, bEnable ? 0 : TBS_ENABLESELRANGE, bEnable ? TBS_ENABLESELRANGE : 0 ); }
};


}; // namespace CSTB


# endif // include guard