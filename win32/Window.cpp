# include "Window.h"
# include <winuser.h>


//# define CSTB_WINDOW_USE_MAP

using namespace CSTB;


HHOOK	hWindowCreationHook = 0;
Window*	pWindowBeingCreated = 0;

# define CSTB_WINDOW_USE_MAP
# ifdef CSTB_WINDOW_USE_MAP
# include <map>
typedef std::map< HWND, Window* > WindowMap;
static WindowMap g_WindowMap;
# endif


ATOM CSTBWindowAtom = NULL;
BOOL CSTBWindowInited = FALSE;
LRESULT CALLBACK CreationHookProcedure( int nCode, WPARAM wParam, LPARAM lParam );




Window::Window()
{
	hWnd = NULL;
	//hMenu = NULL;
	m_hAccel = NULL;
	m_OriginalProc = NULL;
	m_ptPrevCursor.x = m_ptPrevCursor.y = 0;
	m_closeAction = CLOSE_DEFAULT;
	m_pUserData = NULL;
}



Window::~Window()
{
	if( hWnd && IsWindow( hWnd ) )
		DestroyWindow( hWnd );
	//if( hMenu ) DestroyMenu( hMenu );
}



int Window::EnableCreationHook()
{
	pWindowBeingCreated = this;
	hWindowCreationHook = SetWindowsHookEx( WH_CBT, CreationHookProcedure, 0, ::GetCurrentThreadId() );
	if( hWindowCreationHook == 0 )
		return -1;
	return 0;
}




LRESULT CALLBACK Window::CreationHookProcedure( int nCode, WPARAM wParam, LPARAM lParam )
{
	if( nCode == HCBT_CREATEWND )
	{
		// Connect the new window with the class instance
		if( pWindowBeingCreated )
		{
			if( GetWindowLongPtr( (HWND)wParam, GWLP_WNDPROC ) == (LONG_PTR)Window::StaticMsgProc )
				pWindowBeingCreated->Attach( (HWND)wParam );
			else
				pWindowBeingCreated->Subclass( (HWND)wParam );
		}

		// Remove the hook procedure.
		UnhookWindowsHookEx( hWindowCreationHook );
		hWindowCreationHook = 0;
		pWindowBeingCreated = 0;

		// Return 0 to allow continued creation.
		return 0;
	}
	return CallNextHookEx( hWindowCreationHook, nCode, wParam, lParam );
}





// Registers the window class.
BOOL Window::RegisterClass( HINSTANCE hInstance )
{
	if( CSTBWindowInited )
		return CSTBWindowAtom != NULL;
	CSTBWindowInited = TRUE;

	WNDCLASSEX wc;
	wc.cbSize			= sizeof(WNDCLASSEX);
	wc.style			= 0;
	wc.lpfnWndProc		= Window::StaticMsgProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= hInstance;
	wc.hIcon			= 0;
	wc.hIconSm			= 0;
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= CSTB_WINDOWCLASS;

	if( !RegisterClassEx(&wc) )
		return FALSE;

	return TRUE;
}






BOOL Window::Attach( HWND hNewWnd )
{
	if( hWnd || FromHandle( hNewWnd ) ) return FALSE;
	hWnd = hNewWnd;
# ifdef CSTB_WINDOW_USE_MAP
	g_WindowMap.insert( WindowMap::value_type( hWnd, this ) );
# else
	SetWindowLongPtr( hWnd, GWLP_USERDATA, (LONG_PTR)this );
# endif
	return TRUE;
}






BOOL Window::Detach()
{
	if( !hWnd ) return FALSE;
# ifdef CSTB_WINDOW_USE_MAP
	g_WindowMap.erase( hWnd );
# else
	SetWindowLongPtr( hWnd, GWLP_USERDATA, 0 );
# endif

	if( m_OriginalProc )
	{
		SetWindowLongPtr( hWnd, GWLP_WNDPROC, (LONG_PTR)m_OriginalProc );
		m_OriginalProc = NULL;
	}
	hWnd = NULL;
	return TRUE;
}





// Disable "conversion from LONG to ptr of different size" warnings. VC2005 is bugged.
# pragma warning( push )
# pragma warning( disable: 4311 )
BOOL Window::Subclass( HWND hNewWnd )
{
	if( hWnd || FromHandle( hNewWnd ) )
		return FALSE;
	m_OriginalProc = (WNDPROC)SetWindowLongPtr( hNewWnd, GWLP_WNDPROC, (LONG_PTR)StaticMsgProc );
	if( !m_OriginalProc )
		return FALSE;
	return Attach( hNewWnd );
}
# pragma warning( pop )





Window* Window::FromHandle( HWND hWnd )
{
# ifdef CSTB_WINDOW_USE_MAP
	WindowMap::iterator it = g_WindowMap.find( hWnd );
	if( it == g_WindowMap.end() )
		return NULL;

	Window* pWnd = it->second;
# else
	Window* pWnd = (Window*)GetWindowLongPtr( hWnd, GWLP_USERDATA );
# endif
	return pWnd;
}





BOOL Window::Create( DWORD styleEx, LPCTSTR className, LPCTSTR title, DWORD style,
					int x, int y, int width, int height, HWND parent, HMENU menu, HINSTANCE hInst, void* lParam )
{
	if( !CSTBWindowInited )
		RegisterClass( hInst );

	// If a window is already attached, destroy it.
	if( hWnd && IsWindow( hWnd ) )
		DestroyWindow( hWnd );

	// Create the window - we set up a hook procedure to connect the class instance with the window handle.
	EnableCreationHook();
	return NULL != CreateWindowEx( styleEx, className, title, style, x, y, width, height,
								parent, menu, hInst, lParam );
}






// static message proc - just marshals messages to their respective window classes.
LRESULT CALLBACK Window::StaticMsgProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	Window* pWnd = Window::FromHandle( hWnd );
	if( !pWnd )
		return DefWindowProc( hWnd, Msg, wParam, lParam );

	LRESULT ret;
	BOOL bHandled = FALSE;

	if( !bHandled )
		ret = pWnd->MsgProc( Msg, wParam, lParam );

	if( Msg == WM_NCDESTROY )
	{
		pWnd->Detach();
		pWnd->OnNCDestroy();
	}
	return ret;
}





// static method
// If wait is true then the function will wait until a message arrives.
// This checks the messages for all windows in the app.
// The function returns true when a WM_QUIT message is received.
BOOL Window::CheckMessages( bool wait, HWND hWnd )
{
	if( wait ) WaitMessage();

	MSG msg;
	while( ::PeekMessage( &msg, hWnd, 0, 0, PM_REMOVE ) )
	{
		if( msg.message == WM_QUIT )
		{
			// Allow menus to receive this message before we return.
			::TranslateMessage( &msg );
			::DispatchMessage( &msg );
			return FALSE;
		}

		// Let window and parents translate the message.
		int isTranslated = 0;
		HWND hwnd = msg.hwnd;
		while( hwnd )
		{
			Window* wnd = FromHandle( hwnd );
			if( wnd )
			{
				if( 0 != (isTranslated = wnd->TranslateMessage( &msg )) )
					break;
			}
			hwnd = GetParent( hwnd );
		}

		if( isTranslated != 1 )
		{
			::TranslateMessage( &msg ); // Translate key messages to character messages.
			::DispatchMessage( &msg );	// Send the messages to the window procedure.
		}
	}
	return TRUE;
}





LRESULT Window::MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
{
	BOOL bHandled = FALSE;
	LRESULT ret = 0;

# ifdef CSTB_WINDOW_USE_EVENTS
	for( CSTB::HCALLBACK hCB = eventMsgProc.GetCallbackList().begin();
		hCB != eventMsgProc.GetCallbackList().end(); )
	{
		CSTB::HCALLBACK hCurCB = hCB++;
		ret = eventMsgProc.GetDelegate( hCurCB )( *this, Msg, wParam, lParam, bHandled );
		if( bHandled )
			return ret;
	}
# endif

	if( this->m_OriginalProc == NULL )
	{
		// Don't do mouse processing for subclassed windows by default.
		ret = ProcessMouse( Msg, wParam, lParam, bHandled );
		if( bHandled )
			return ret;
	}


	switch( Msg )
	{
	case WM_CREATE: if( !OnCreate( (CREATESTRUCT*)lParam ) ) return 0; break;
	case WM_DESTROY: OnDestroy(); break;
	case WM_CLOSE: if( OnClose() == 0 ) return 0;
	case WM_COMMAND:
		{
			LRESULT ret = OnCommand( (HWND)lParam, LOWORD(wParam), HIWORD(wParam) );
			if( ret )
				return ret;
		}
		break;

	case WM_NOTIFY:
		{
			LRESULT ret = OnNotify( wParam, (NMHDR*)lParam, bHandled );
			if( bHandled )
				return ret;
		}
		break;

	case WM_SIZE:
		OnSize();
		break;
	}
	return DefWndProc( Msg, wParam, lParam );
}






LRESULT Window::ProcessMouse( UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
	bHandled = FALSE;
	switch( Msg )
	{
	// TODO: Consider WM_XBUTTON1DOWN et al.
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
		{
			POINT ptCursor; GetCursorPos( &ptCursor );
			ScreenToClient( hWnd, &ptCursor );
			SetCapture( hWnd );
			if( OnMousePress( Msg, wParam, ptCursor ) )
				bHandled = TRUE;
		}
		break;


	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
		{
			POINT ptCursor; GetCursorPos( &ptCursor );
			ScreenToClient( hWnd, &ptCursor );
			if( OnMouseRelease( Msg, wParam, ptCursor ) )
				bHandled = TRUE;

			// TODO: Consider MK_XBUTTON1
			if( GetCapture() == hWnd && !(wParam & MK_LBUTTON) && !(wParam & MK_RBUTTON) && !(wParam & MK_MBUTTON) )
				ReleaseCapture();
		}
		break;


	case WM_MOUSEMOVE:
		{
			// This is to have the window receive WM_MOUSELEAVE and WM_MOUSEHOVER, which are cool!
			TRACKMOUSEEVENT tme = {0};
			tme.cbSize = sizeof(TRACKMOUSEEVENT);
			tme.hwndTrack = hWnd;

			tme.dwHoverTime = HOVER_DEFAULT;
			tme.dwFlags = TME_LEAVE | TME_HOVER;
			TrackMouseEvent( &tme );
		}
		{
			POINT ptCursor; GetCursorPos( &ptCursor );
			ScreenToClient( hWnd, &ptCursor );
			if( OnMouseMove( wParam, ptCursor, m_ptPrevCursor ) )
				bHandled = TRUE;
			m_ptPrevCursor = ptCursor;
		}
		break;


	case WM_CAPTURECHANGED:
		{
			POINT ptCursor; GetCursorPos( &ptCursor );
			ScreenToClient( hWnd, &ptCursor );
			m_ptPrevCursor = ptCursor;
		}
		if( hWnd != (HWND)lParam )
			bHandled = OnMouseCaptureLost( (HWND)lParam );
		break;


	case WM_MOUSEHOVER:
		{
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			bHandled = OnMouseHover( pt, wParam );
		}
		break;


	case WM_MOUSELEAVE:
		bHandled = OnMouseLeave();
		break;
	}

	if( bHandled )
		return DefWndProc( Msg, wParam, lParam );
	return 0;
}