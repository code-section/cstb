# pragma once

/*
Simple text output window, based on an edit control.
Recent changes:
* varagrs (printf formatting) is not the default now. You are encouraged to use a better alternative for formatting strings (like the fmt library).
* Removed file loggging.
* RAII

Copyright Adel Amro - http://code-section.com
*/




# include <windows.h>

//# ifndef NO_STD_STRING
//# include <string>
//# endif



namespace CSTB
{


class LogWindow
{
protected:
	HWND hWnd = NULL;
	HWND hEdit = NULL;
	//HANDLE hFile;
	HFONT hFont = NULL;
	unsigned indentation = 0;
	UINT codePage = CP_ACP;

private:
	// Non-copyable.
	LogWindow(const LogWindow& rhs) = delete;
	LogWindow& operator = (const LogWindow& rhs) = delete;

public:

	enum class Error
	{
		REGISTER_WINDOW_CLASS,
		CREATE_WINDOW,
		CREATE_EDIT_CONTROL
	};
	LogWindow(const wchar_t* title, HINSTANCE hInst, HWND parent = NULL, UINT codePage = CP_ACP, bool topMost = false);
	~LogWindow();


	void Clear();

	void Write( const char* msg, bool newline=true );
	void Write( const wchar_t* msg, bool newline = true);
	void operator () (const char* msg, bool newline = true) { Write(msg, newline); }
	void operator () (const wchar_t* msg, bool newline = true) { Write(msg, newline); }

//# ifndef NO_STD_STRING
//	void Write(const std::string& msg, bool newline = true) { Write(msg.c_str(), newline); }
//	void Write(const std::wstring& msg, bool newline = true) { Write(msg.c_str(), newline); }
//	void operator () (const std::string& msg, bool newline = true) { Write(msg.c_str(), newline); }
//# endif

	void printf(const char* msg, ...);
	void printf(const wchar_t* msg, ...);

	void Indent()	{ indentation++; }							// Add an indentation level.
	void Unindent()	{ if( indentation != 0 ) indentation--; }	// Remove an indentation level.

	operator HWND() const { return hWnd; }

	// Utility object for easier indentation.
	// The object will indent the log window when it's created, and unindent it when it goes out of scope.
	struct ScopeIndent
	{
		LogWindow& logWindow;
		ScopeIndent(LogWindow& _logWindow, const char* msg = 0, bool newline=true ) : logWindow(_logWindow) { logWindow.Indent(); if (msg) logWindow.Write(msg, newline);  }
		ScopeIndent(LogWindow& _logWindow, const wchar_t* msg = 0, bool newline=true ) : logWindow(_logWindow) { logWindow.Indent(); if (msg) logWindow.Write(msg, newline); }
		~ScopeIndent() { logWindow.Unindent(); }
	};

protected:
	LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam );
	static LRESULT CALLBACK StaticMsgProc( HWND, UINT, WPARAM, LPARAM );
	static BOOL RegisterClass( HINSTANCE );
};


};


