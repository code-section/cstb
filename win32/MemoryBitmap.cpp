

# ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
# endif

# include "MemoryBitmap.h"
# include "Win32Util.h"
# include <stdio.h> // For file stuff.
# include <tchar.h>



namespace CSTB
{



BOOL MemoryBitmap::Create( UINT _width, UINT _height, UINT _bpp )
{
	Cleanup();

	ZeroMemory( &bmi, sizeof( BITMAPINFO ) );
	bmi.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
	bmi.bmiHeader.biWidth = _width;
	bmi.bmiHeader.biHeight = 0 - _height; // With a negative height so that the final image is flipped correctly.
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount = _bpp;

	HDC hdc = NULL;
	hbmp = CreateDIBSection( hdc, &bmi, DIB_RGB_COLORS, (VOID**)&pixels, NULL, 0 );

	if( hbmp == NULL )
		return FALSE;

	width = _width;
	height = _height;
	bpp = _bpp;

	rowSize = (int)DwordAlign( bpp/8 * width );
	//bmi.bmiHeader.biSizeImage = rowSize * height;
	return TRUE;
}




BOOL MemoryBitmap::Blit( HDC hDC, INT xDest, INT yDest, INT width, INT height, INT xSrc, INT ySrc, DWORD dwRop )
{
	HDC hMemoryDC = CreateCompatibleDC( NULL );
	HBITMAP hOldBitmap = (HBITMAP)SelectObject( hMemoryDC, hbmp );
	BOOL bRet = BitBlt(
				hDC,		// DC of target window
				xDest,		// x coord
				yDest,		// y coord
				width,		// Width of area to copy
				height,		// Height of area to copy
				hMemoryDC,	// Source DC
				xSrc,		// x coord in source dc
				ySrc,		// y coord in source dc
				dwRop		// blit type.
				);
	SelectObject( hMemoryDC, hOldBitmap );
	DeleteDC( hMemoryDC );

	return bRet;
}




BOOL MemoryBitmap::Load( LPCTSTR sFileName )
{
	if( sFileName == NULL || sFileName[0] == 0 )
	{
		SetLastError( ERROR_BAD_ARGUMENTS );
		return NULL;
	}
	DIBSECTION ds = {0};
	BYTE* pBytes = NULL;

	HBITMAP hBM = (HBITMAP)LoadImage( NULL, sFileName, IMAGE_BITMAP, 0, 0,
								LR_CREATEDIBSECTION | LR_LOADFROMFILE );
	if( hBM == NULL )
		return NULL;

	// First get bitmap info.
	if( NULL == GetObject( hBM, sizeof(DIBSECTION), (VOID*)&ds ) )
	{
		DeleteObject( hBM );
		return NULL;	// GetObject() calls SetLastError() by itself.
	}
	if( ds.dsBm.bmPlanes != 1 || ds.dsBm.bmBits == NULL )
	{
		DeleteObject( hBM );
		SetLastError( ERROR_BAD_FORMAT );
		return NULL;
	}
	Create( ds.dsBm.bmWidth, ds.dsBm.bmHeight, 32 );

	switch( ds.dsBm.bmBitsPixel )
	{
		case 24:
			// In 24-bit bitmaps, RGB values are stored backwards (BGR).
			for( int y=0; y<height; y++ )
			{
				pBytes = (BYTE*)ds.dsBm.bmBits + y * ds.dsBm.bmWidthBytes;
				for( int x=0; x<width; x++ )
				{
					SetPixel32( (unsigned)x, (unsigned)y, 255, *(pBytes+2), *(pBytes+1), *(pBytes+0) );
					pBytes += 3;
				}
			}
			break;


		case 32:
			for( int y=0; y<height; y++ )
			{
				pBytes = (BYTE*)ds.dsBm.bmBits + y * ds.dsBm.bmWidthBytes;
				for( int x = 0; x<width; x++ )
				{
					SetPixel32( (unsigned)x, (unsigned)y, *(DWORD*)pBytes );
					pBytes += 4;
				}
			}
			break;

		default:
			// Unsupported color format.
			SetLastError( ERROR_BAD_FORMAT );
			Clear();
	}

	DeleteObject( hBM );
	return TRUE;
}




BOOL MemoryBitmap::Save( const PBITMAPINFO pbmi, LPCTSTR sFileName, void* pPixels )
{
	FILE* pFile = _tfopen( sFileName, TEXT("wb") );
	if( pFile == NULL )
		return FALSE;

	INT nWidth = pbmi->bmiHeader.biWidth;
	INT nHeight = pbmi->bmiHeader.biHeight;
	if( nHeight < 0 )
		nHeight = -nHeight;

	INT rowSizeUnaligned = pbmi->bmiHeader.biBitCount/8 * pbmi->bmiHeader.biWidth;
	INT rowSize = (INT)DwordAlign( rowSizeUnaligned );

	BITMAPFILEHEADER bmfh;
	bmfh.bfType = 0x4d42;	// 0x42 = "B" 0x4d = "M", says MSDN.

	// compute size of bitmap file.
	bmfh.bfSize =	sizeof( BITMAPFILEHEADER ) +
					sizeof( BITMAPINFO ) +
					rowSize;
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfOffBits = sizeof( BITMAPFILEHEADER ) + sizeof( BITMAPINFO );

	if( 0 == fwrite( (VOID*)&bmfh, sizeof( BITMAPFILEHEADER ), 1, pFile ) )
	{
		//g_Log( "Failed to write file info header!" );
		fclose( pFile );
		return FALSE;
	}


	// NOTE: Some programs have problems loading bitmaps whose height is negative.
	// Therefore, this function will save the height as positive, causing the output
	// file to be vertically flipped.
	LONG oldHeight = pbmi->bmiHeader.biHeight;
	if( oldHeight < 0 )
		pbmi->bmiHeader.biHeight = -oldHeight;

	if( 0 == fwrite( (VOID*)pbmi, sizeof( BITMAPINFO ), 1, pFile ) )

	pbmi->bmiHeader.biHeight = oldHeight;



	// Now write the pixels:
	INT paddingBytes = rowSize - rowSizeUnaligned;
	DWORD dwZero = 0;

	BYTE* pBytes = (BYTE*)pPixels;
	for( INT y=0; y<nHeight; y++ )
	{
		fwrite( pBytes, rowSize - paddingBytes, 1, pFile );
		if( paddingBytes != 0 )
			fwrite( &dwZero, 1, paddingBytes, pFile );
		pBytes += rowSize;
	}
	fclose( pFile );

	return TRUE;
}


}; // namespace CSTB