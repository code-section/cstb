/// Custom win32 control that is a color box. The user clicks it, it shows the standard color dialog.
/// -Adel Amro - http://code-section.com

# ifndef CSTB_COLORBOX_H
# define CSTB_COLORBOX_H


# include <windows.h>
# include <commctrl.h>



/// @defgroup ColorBox
/// @{

/// Window class name for the color box control. Pass this to CreateWindowEx after calling ColorBox_Init().
# define WC_COLORBOX			TEXT( "CSTB_COLORBOX" )


/// Notifications. In addition to sending those in the form of WM_NOTIFY messages to the parent, the control
/// will also send CBXN_COLORCHANGE with a WM_COMMAND message to the parent when the color changes.
# define CBXN_COLORCHANGING		0x0100 ///< The color is about to change. Return nonzero to prevent it. @see NM_COLORBOX
# define CBXN_COLORCHANGE		0x0200 ///< The color has changed. Return value ignored. @see NM_COLORBOX
# define CBXN_GETTEXT			0x0400 ///< Allow parent to set the text that the control draws. See COLORBOX_TEXTMODE.



/// Color box notification structure sent by the color box control to notify the
/// parent window about color change events. Sent in the lParam parameter for the WM_NOTIFY message.
struct NM_COLORBOX
{
	NMHDR hdr;
	COLORREF oldColor;
	COLORREF newColor;
};


/// Creates a color box window. @deprecated: use CreateWindowEx() with WC_COLORBOX instead.
HWND ColorBox_Create( HWND hParent, UINT id, INT x, INT y, INT cx, INT cy );

/// Retrieves the color of the control.
COLORREF ColorBox_GetColor( HWND hWnd );


/// Sets the color of the control
VOID ColorBox_SetColor( HWND hWnd, COLORREF c, BOOL bRedraw = TRUE, BOOL bNotifyParent = FALSE );

/// Shows the "choose color" common dialog that enables the user to select a color.
VOID ColorBox_PickColor( HWND hWnd );


/// Text mode for the text drawn inside the control.
enum COLORBOX_TEXTMODE
{
	CBXT_NONE = 0,
	CBXT_FLOAT, // 01
	CBXT_01 = CBXT_FLOAT, ///< Color value is formatted using "%.2f, %6.2f, %6.2f".
	CBXT_BYTE,
	CBXT_255 = CBXT_BYTE, ///< Color value is formatted using "%d, %3d, %3d". This is the default.
	/// Parent decides. When the control is being painted, it wills end a CBXN_GETTEXT notification to the
	/// parent window, and it should return a string pointer used for painting (or NULL).
	CBXT_CALLBACK 
};

/// See COLORBOX_TEXTMODE.
COLORBOX_TEXTMODE ColorBox_GetTextMode( HWND hWnd );

/// Sets the mode used to draw the color value. See COLORBOX_TEXTMODE. Invalidates but doesn't update the window.
VOID ColorBox_SetTextMode( HWND hWnd, COLORBOX_TEXTMODE newMode );

/// Returns the string drawn in the control. Pointer should not be stored. This can be provided either by the
/// control itself or its parent (see COLORBOX_TEXTMODE).
LPCTSTR ColorBox_GetText( HWND hWnd );

/// Returns the color used by the control to draw text. Useful if the control is subclassed to draw more text or something.
COLORREF ColorBox_GetTextColor( HWND hWnd );




/// Registers the control's window class. After this function returned successfully, color box controls
/// can be created with CreateWindowEx() and passing WC_COLORBOX as the winow class name.
BOOL ColorBox_Init( HINSTANCE hInstance );


/// Unregisters the color box control's window class.
VOID ColorBox_Uninit( HINSTANCE hInstance );

/// @} end ColorBox group.


# endif // include guard