/*
TODO:
1. Almost all global variables should be regular properties. Otherwise multiple instances can use the same buffers and overwrite data.
2. If the string length is greater than the buffer size, print it in chunks instead of trimming.
3. Verify correctness when string length is greater than or equal to the buffer size.
*/

# include "LogWindow.h"
# include <stdio.h>


namespace CSTB
{


ATOM LogWindowAtom = NULL;
BOOL LogWindowInited = FALSE;
LogWindow* pAttachInstance = NULL; // The instance that needs to be attached to the window.
const unsigned int MAX_LINE = 4096;
static WCHAR buff[MAX_LINE];
static WCHAR conversionBuffer[MAX_LINE];
LPCTSTR WC_LOGWINDOW = TEXT("CSTB_LogWindow");


LogWindow*	GetClassPointer( HWND hWnd )					{ return (LogWindow*)GetWindowLongPtr( hWnd, 0 ); }
VOID		SetClassPointer( HWND hWnd, LogWindow* pLW )	{ SetWindowLongPtr( hWnd, 0, (LONG_PTR)pLW ); }


// Marshal messages to the message handler method in the LogWindow class.
LRESULT CALLBACK LogWindow::StaticMsgProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	if( Msg == WM_NCCREATE )
	{
		SetClassPointer( hWnd, pAttachInstance );
		pAttachInstance->hWnd = hWnd;
	}
	LogWindow* pLogWindow = GetClassPointer( hWnd );
	if( pLogWindow )
		return pLogWindow->MsgProc( Msg, wParam, lParam );
	return DefWindowProc( hWnd, Msg, wParam, lParam );
}



BOOL LogWindow::RegisterClass( HINSTANCE hInstance )
{
	if( LogWindowInited )
		return LogWindowAtom != NULL;
	LogWindowInited = TRUE;

	WNDCLASSEX wc;
	wc.cbSize			= sizeof(WNDCLASSEX);
	wc.style			= 0;
	wc.lpfnWndProc		= LogWindow::StaticMsgProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= sizeof( LogWindow* );
	wc.hInstance		= hInstance;
	wc.hIcon			= 0;
	wc.hIconSm			= 0;
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= NULL;
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= WC_LOGWINDOW;

	LogWindowAtom = RegisterClassEx(&wc);
	if( !LogWindowAtom )
		return FALSE;

	ZeroMemory( buff, MAX_LINE * sizeof(TCHAR) );

	return TRUE;
}



LogWindow::LogWindow(const wchar_t* title, HINSTANCE hInst, HWND parent, UINT _codePage, bool topMost):
	codePage(_codePage)
{
	if (!RegisterClass(hInst))
		throw Error::REGISTER_WINDOW_CLASS;

	pAttachInstance = this;

	hWnd = CreateWindowEx(WS_EX_TOOLWINDOW | (topMost ? WS_EX_TOPMOST : 0),
		WC_LOGWINDOW, title,
		WS_VISIBLE | WS_POPUPWINDOW | WS_SYSMENU | WS_CAPTION | WS_THICKFRAME,
		CW_USEDEFAULT, CW_USEDEFAULT, 350, 220,
		parent, NULL, hInst, NULL);

	if (!hWnd) throw Error::CREATE_WINDOW;

	DWORD style = WS_VISIBLE | WS_CHILD | ES_MULTILINE | ES_READONLY |
		/*ES_AUTOHSCROLL |*/ ES_AUTOVSCROLL /*| WS_HSCROLL*/ | WS_VSCROLL | ES_WANTRETURN;

	hEdit = CreateWindowEx(0, TEXT("Edit"), NULL,
		style, 0, 0, 100, 100, hWnd, (HMENU)1000, hInst, NULL);

	if (!hEdit) throw Error::CREATE_EDIT_CONTROL;

	
	// Create the font.
	NONCLIENTMETRICS ncm;
	ncm.cbSize = sizeof(NONCLIENTMETRICS);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
	hFont = CreateFontIndirect(&ncm.lfMessageFont);
	SendMessage( hEdit, WM_SETFONT, (WPARAM)hFont, 0L );
	//SendMessage(hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0L);

	UINT tabStops = 16;
	SendMessage(hEdit, EM_SETTABSTOPS, 1, (LPARAM)&tabStops);

	SendMessage(hWnd, WM_SIZE, 0, 0);
}


LogWindow::~LogWindow()
{
	DestroyWindow(hWnd);
	DeleteObject(hFont);
}



void LogWindow::Clear()
{
	SendMessage( hEdit, WM_SETTEXT, 0, 0 );
	//SendMessage( hEdit, EM_SETSEL, 0, -1 );
	//SendMessage( hEdit, WM_CLEAR, 0, 0 );
}



VOID AppendToEdit( HWND hEdit, const WCHAR* text )
{
	if( !text || text[0] == 0 )
		return;

	INT start, end;
	SendMessage( hEdit, EM_GETSEL, (WPARAM)&start, (LPARAM)&end );
	INT length = (INT)SendMessage( hEdit, WM_GETTEXTLENGTH, 0L, 0L );
	SendMessage( hEdit, EM_SETSEL, length, length );
	SendMessage( hEdit, EM_REPLACESEL, FALSE, (LPARAM)text );
	SendMessage( hEdit, EM_SETSEL, start, end );
}



void LogWindow::printf(const wchar_t* msg, ...)
{
	if (!msg || msg[0] == 0) return;
	va_list vList;
	va_start(vList, msg);
	_vsnwprintf(buff, MAX_LINE, msg, vList);
	AppendToEdit(hEdit, buff);
}



void LogWindow::printf( const char* msg, ... )
{
	if( !msg || msg[0] == 0 ) return;
	va_list vList;
	va_start( vList, msg );
	MultiByteToWideChar( codePage, 0, msg, -1, conversionBuffer, MAX_LINE );
	const wchar_t* wmsg = conversionBuffer;
	_vsnwprintf( buff, MAX_LINE, wmsg, vList );
	AppendToEdit( hEdit, buff );
}



void LogWindow::Write(const char* msg, bool newline)
{
	MultiByteToWideChar(codePage, 0, msg, -1, conversionBuffer, MAX_LINE);
	const wchar_t* wmsg = conversionBuffer;
	Write(wmsg, newline);
}



void LogWindow::Write(const wchar_t* msg, bool newline)
{
	if (indentation == 0 && !newline)
	{
		AppendToEdit(hEdit, msg);
		return;
	}

	// Prepend leading tab spaces.
	unsigned prefixLen = 0;
	for (unsigned i = 0; i < indentation; i++)
		buff[prefixLen++] = L'\t';

	wcsncpy(buff + prefixLen, msg, MAX_LINE - prefixLen );

	// Append the new line character sequence.
	if (newline)
	{
		int len = wcslen(msg) + prefixLen;
		if (MAX_LINE - len > 2)
		{
			buff[len++] = L'\r';
			buff[len++] = L'\n';
			buff[len] = 0;
		}
	}
	AppendToEdit(hEdit, buff);
}



LRESULT LogWindow::MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
{
	switch( Msg )
	{

	case WM_CLOSE:
		ShowWindow( hWnd, SW_HIDE );
		return 0;


	case WM_DESTROY:
	{
		LRESULT ret = DefWindowProc(hWnd, Msg, wParam, lParam);
		hWnd = NULL;
		hEdit = NULL;
		return ret;
	}
	break;

	case WM_SIZE:
		if( hEdit )
		{
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			MoveWindow( hEdit, 0, 0, rClient.right - rClient.left, rClient.bottom - rClient.top, TRUE );
		}
		break;
	}
	return DefWindowProc( hWnd, Msg, wParam, lParam );
}


}; // namespace CSTB