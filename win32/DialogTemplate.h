/**
The DialogTemplate class facilitates creating a dialog template in memory.

The code is adapted from the code posted on flipcode by Max McGuire.
**/


# ifndef CSTB_DIALOG_TEMPALTE_H
# define CSTB_DIALOG_TEMPLATE_H

# include <windows.h>
# include <commctrl.h>


namespace CSTB
{



class DialogTemplate
{
public:
	DialogTemplate()
	{
		m_pDialogTemplate = NULL;
		m_nTotalBufferLength = 0;
		m_nUsedBufferLength = 0;
	}

	DialogTemplate( LPCTSTR sCaption, DWORD dwStyle, DWORD dwExtendedStyle, INT x, INT y,
				INT cx, INT cy, LPCTSTR sFontFace = NULL, WORD wFontSize = 8 )
	{
		m_pDialogTemplate = NULL;
		m_nTotalBufferLength = 0;
		m_nUsedBufferLength = 0;
		Create( sCaption, dwStyle, dwExtendedStyle, x, y, cx, cy, sFontFace, wFontSize );
	}

	BOOL Create( LPCTSTR sCaption, DWORD dwStyle, DWORD dwExtendedStyle, INT x, INT y,
				INT cx, INT cy, LPCTSTR sFontFace = NULL, WORD wFontSize = 8 )
	{
		m_nUsedBufferLength = sizeof( DLGTEMPLATE );
		m_nTotalBufferLength = m_nUsedBufferLength;
		m_pDialogTemplate = new BYTE[ m_nTotalBufferLength ];

		DLGTEMPLATE* pdt = (DLGTEMPLATE*)m_pDialogTemplate;

		pdt->style = dwStyle;
		if( sFontFace != NULL )
			pdt->style |= DS_SETFONT;

		pdt->x = (SHORT)x;
		pdt->y = (SHORT)y;
		pdt->cx = (SHORT)cx;
		pdt->cy = (SHORT)cy;
		pdt->cdit = 0;
		pdt->dwExtendedStyle = dwExtendedStyle;

		WORD wNull = 0;
		AppendData( &wNull, 2 );		// No menu.
		AppendData( &wNull, 2 );		// No special class.

		AppendString( sCaption );
		if( sFontFace != NULL )
		{
			AppendData( &wFontSize, sizeof(WORD) );
			AppendString( sFontFace );
		}
		return TRUE;
	}


	// NOTE: Use FindAtom() to get the atom number of a specified control using
	// its name. For example, to create a button, you can either pass 0x0080 in
	// the wType parameter, or you can use FindAtom( "BUTTON" ).
	VOID AddControl( LPCTSTR sClass, LPCTSTR sCaption, DWORD dwStyle, DWORD dwExtendedStyle,
		INT x, INT y, INT cx, INT cy, WORD wID )
	{
		DLGITEMTEMPLATE item;
		AlignData( sizeof(DWORD) );
		item.x = (SHORT)x;
		item.y = (SHORT)y;
		item.cx = (SHORT)cx;
		item.cy = (SHORT)cy;
		item.id = wID;
		item.style = dwStyle;
		item.dwExtendedStyle = dwExtendedStyle;
		AppendData( &item, sizeof(DLGITEMTEMPLATE) );
		AppendString( sClass );
		AppendString( sCaption );
		// No creation data.
		WORD wCreationDataLength = 0;
		AppendData( &wCreationDataLength, sizeof( WORD ) );

		((DLGTEMPLATE*)m_pDialogTemplate)->cdit++;
	}

	~DialogTemplate()
	{
		delete [] m_pDialogTemplate;
	}

	operator const DLGTEMPLATE* () const
	{
		return (DLGTEMPLATE*) m_pDialogTemplate;
	}


protected:

	BYTE*	m_pDialogTemplate;
	INT		m_nTotalBufferLength;
	INT		m_nUsedBufferLength;


	VOID AlignData( int nSize )
	{
		INT nPaddingSize = m_nUsedBufferLength % nSize;
		if( nPaddingSize != 0 )
		{
			EnsureSpace( nPaddingSize );
			m_nUsedBufferLength += nPaddingSize;
		}
	}


	VOID AppendString( LPCTSTR s )
	{
# ifdef UNICODE
		AppendData( (VOID*)s, (lstrlen( s )+1) * sizeof(TCHAR) );
# else
		INT nLength = MultiByteToWideChar( CP_ACP, 0, s, -1, NULL, 0 );
		WCHAR* pWideString = new WCHAR[ nLength ];
		MultiByteToWideChar( CP_ACP, 0, s, -1, pWideString, nLength );
		AppendData( pWideString, nLength * sizeof( WCHAR ) );
		delete [] pWideString;
# endif
	}


	VOID AppendData( const VOID* pData, INT nDataLength )
	{
		EnsureSpace( nDataLength );
		memcpy( m_pDialogTemplate + m_nUsedBufferLength, pData, nDataLength );
		m_nUsedBufferLength += nDataLength;
	}


	VOID EnsureSpace( INT nDataLength )
	{
		if( nDataLength + m_nUsedBufferLength > m_nTotalBufferLength )
		{
			//m_nTotalBufferLength += nDataLength * 2;
			m_nTotalBufferLength += nDataLength*2 > 1024 ? nDataLength*2 : 1024;
			BYTE* pNewBuffer = new BYTE[ m_nTotalBufferLength ];
			memcpy( (VOID*)pNewBuffer, m_pDialogTemplate, m_nUsedBufferLength );
			delete [] m_pDialogTemplate;
			m_pDialogTemplate = pNewBuffer;
		}
	}
};


}; // namespace



# endif // include guard