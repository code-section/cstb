// Implementation file for the Angel3D spin box control


# include <windows.h>
# include "SpinBoxCtrl.h"
# include "Win32Util.h"
# include "UXThemeProxy.h"
# include <strsafe.h>


// Global variables.
static HFONT	g_hGlyphFont = NULL;
static HFONT	g_hBigGlyphFont = NULL;

static HWND		g_hDragSpinBox = NULL; // The spinbox control that the user is dragging.
static INT		g_nDragDir = 0; // 0 = none, 1 = horizontal, 2 = vertical.
static POINT	g_ptMouseStart = {0,0};
static POINT	g_ptPrevCursor = {0};
static HCURSOR	g_hOldCursor = NULL;

static UXThemeProxy	g_uxTheme;

#ifndef CLEARTYPE_QUALITY
#define CLEARTYPE_QUALITY  5
#endif




////////////////////////////////////////////////////
// Structure which holds the spin control's information.
struct SpinboxInfo
{
	HWND hWnd;
	HWND hNotifyWnd;
	HTHEME hButtonTheme;
	UINT dragPixels;
	INT layout;
	INT hotPart;
	BOOL bHot;
	BOOL bDragOnly;

	struct TPart
	{
		RECT rect;
		BOOL disabled;
		TPart()
		{
			SetRect( &rect, 0, 0, 0, 0 );
			disabled = FALSE;
		}
	} increment, decrement, drag;


	SpinboxInfo( HWND _hWnd )
	{
		hWnd = _hWnd;
		hNotifyWnd = NULL;
		dragPixels = 1;
		hButtonTheme = NULL;
		layout = SBLAYOUT_HORIZONTAL;
		hotPart = SBP_NONE;
		bHot = FALSE;
		bDragOnly = TRUE; // For now, until the side buttons are functional (we don't need them now).
	}
};




// Disable "conversion from LONG to ptr of different size" warnings.
# pragma warning( push )
# pragma warning( disable: 4311 )
# pragma warning( disable: 4312 )

SpinboxInfo* GetControlData( HWND hWnd )			{ return (SpinboxInfo*)GetWindowLongPtr( hWnd, 0 ); }
VOID SetControlData( HWND hWnd, SpinboxInfo* pSBI )	{ SetWindowLongPtr( hWnd, 0, (LONG_PTR)pSBI ); }

# pragma warning( pop )





VOID SpinBox_GetRects( HWND hSpinBox, RECT* prcIncrement, RECT* prcDecrement, RECT* prcDrag )
{
	SpinboxInfo* pSBI = GetControlData( hSpinBox );
	if( !pSBI ) return;
	
	if( prcIncrement ) *prcIncrement = pSBI->increment.rect;
	if( prcDecrement ) *prcDecrement = pSBI->decrement.rect;
	if( prcDrag ) *prcDrag = pSBI->drag.rect;
}



INT SpinBox_GetLayout( HWND hSpinBox ) { return GetControlData( hSpinBox )->layout; }

BOOL SpinBox_IsDragging( HWND hSpinBox ) { return g_hDragSpinBox && g_hDragSpinBox == hSpinBox; }

VOID SpinBox_EndDragging( HWND hSpinBox, BOOL bNotify )
{
	if( !SpinBox_IsDragging( hSpinBox ) )
		return;

	g_hDragSpinBox = NULL;
	SetCursor( g_hOldCursor );
	// Update the window to reflect state change (button is now unpressed).
	InvalidateRect( hSpinBox, NULL, TRUE );
	UpdateWindow( hSpinBox );

	if( bNotify && SpinBox_GetNotifyWnd( hSpinBox ) )
	{
		NMSPINBOX nm = {0};
		nm.hdr.hwndFrom = hSpinBox;
		nm.hdr.idFrom = GetWindowLong( hSpinBox, GWL_ID );
		nm.hdr.code = SBN_DRAGEND;
		SendMessage( SpinBox_GetNotifyWnd( hSpinBox ), WM_NOTIFY, (WPARAM)nm.hdr.idFrom, (LPARAM)&nm );
	}
}



VOID SpinBox_SetNotifyWnd( HWND hSpinBox, HWND hNotifyWnd ) { GetControlData( hSpinBox )->hNotifyWnd = hNotifyWnd; }
HWND SpinBox_GetNotifyWnd( HWND hSpinBox ) { return GetControlData( hSpinBox )->hNotifyWnd; }


BOOL SpinBox_IsDragOnly( HWND hSpinBox ) { return GetControlData( hSpinBox )->bDragOnly; }

VOID SpinBox_SetDragOnly( HWND hSpinBox, BOOL bDragOnly )
{
	if( SpinBox_IsDragOnly( hSpinBox ) == bDragOnly )
		return;
	GetControlData( hSpinBox )->bDragOnly = bDragOnly;
	InvalidateRect( hSpinBox, NULL, TRUE );
	UpdateWindow( hSpinBox );
}



UINT SpinBox_GetDragPixels( HWND hSpinBox ) { return GetControlData( hSpinBox )->dragPixels; }
VOID SpinBox_SetDragPixels( HWND hSpinBox, UINT dragPixels ) { GetControlData( hSpinBox )->dragPixels = MAX( 1, dragPixels ); }





VOID SpinBox_CalculateLayout( HWND hSpinBox )
{
	SpinboxInfo* pSBI = GetControlData( hSpinBox );
	if( !pSBI ) return;

	RECT rClient;
	GetClientRect( hSpinBox, &rClient );
	RECT& rInc = pSBI->increment.rect;
	RECT& rDec = pSBI->decrement.rect;
	RECT& rDrag = pSBI->drag.rect;

	if( RECTWIDTH(rClient) >= RECTHEIGHT(rClient) )
		pSBI->layout = SBLAYOUT_HORIZONTAL;
	else
		pSBI->layout = SBLAYOUT_VERTICAL;

	//INT buttonSize = GetSystemMetrics( pSBI->bHorizontal ? SM_CXHSCROLL : SM_CXVSCROLL );
	INT buttonSize = SpinBox_IsDragOnly( hSpinBox ) ? 0 : 12;
	if( pSBI->layout == SBLAYOUT_HORIZONTAL )
	{
		// Place the button parts next to each other.
		SetRect( &rDec, rClient.left, rClient.top, buttonSize, rClient.bottom );
		SetRect( &rInc, rClient.right - buttonSize, rClient.top, rClient.right, rClient.bottom );
		SetRect( &rDrag, rDec.right, rClient.top, rInc.left, rClient.bottom );
	}
	else
	{
		// Orient them in a stack.
		SetRect( &rInc, rClient.left, rClient.top, rClient.right, buttonSize );
		SetRect( &rDec, rClient.left, rClient.bottom - buttonSize, rClient.right, rClient.bottom );
		SetRect( &rDrag, rClient.left, rInc.bottom, rClient.right, rDec.top );
	}
}



// Returns the part at the specified point.
INT SpinBox_DoHitTest( HWND hSpinBox, POINT pt )
{
	RECT rClient, rIncrement, rDecrement, rDrag;
	GetClientRect( hSpinBox, &rClient );
	if( !PtInRect( &rClient, pt ) )
		return SBP_NONE;

	SpinBox_GetRects( hSpinBox, &rIncrement, &rDecrement, &rDrag );
	if( PtInRect( &rDrag, pt ) ) return SBP_DRAG;
	if( PtInRect( &rIncrement, pt ) ) return SBP_INCREMENT;
	if( PtInRect( &rDecrement, pt ) ) return SBP_DECREMENT;
	return SBP_NONE;
}







VOID DrawSpinBoxBGThemed( SpinboxInfo* pSBI, RECT* prcClient, RECT* prcPaint, HDC hdc )
{
	FillRect( hdc, prcClient, (HBRUSH)(COLOR_BTNFACE+1) );
	if( !pSBI->hButtonTheme )
		return;
	/*if( g_uxTheme.IsThemeBackgroundPartiallyTransparent( pSBI->hButtonTheme, BP_PUSHBUTTON, PBS_NORMAL ) )
		g_uxTheme.DrawThemeParentBackground( pSBI->hWnd, hdc, prcPaint );*/
	INT iState = PBS_NORMAL;
	if( !IsWindowEnabled( pSBI->hWnd ) )
		iState = PBS_DISABLED;
	else if( SpinBox_IsDragging(pSBI->hWnd) )
		iState = PBS_PRESSED;
	else if( pSBI->bHot )
		iState = PBS_HOT;
	g_uxTheme.DrawThemeBackground( pSBI->hButtonTheme, hdc, BP_PUSHBUTTON, iState, prcClient, prcPaint );
}




VOID DrawSpinBoxBG( SpinboxInfo* pSBI, RECT* prcClient, HDC hdc )
{
	UINT uState = DFCS_BUTTONPUSH;
	if( !IsWindowEnabled( pSBI->hWnd ) )
		uState |= DFCS_INACTIVE;
	else if( SpinBox_IsDragging( pSBI->hWnd ) )
		uState |= DFCS_PUSHED;
	else if( pSBI->bHot )
		uState |= DFCS_HOT;
	DrawFrameControl( hdc, prcClient, DFC_BUTTON, uState );
}





VOID DrawSpinBox( SpinboxInfo* pSBI, RECT* prcClient, HDC hDC )
{
	HGDIOBJ hOldFont = SelectObject( hDC, g_hGlyphFont );
	INT oldBkMode = SetBkMode( hDC, TRANSPARENT );
	COLORREF oldColor = SetTextColor( hDC, GetSysColor( COLOR_BTNTEXT ) );

	RECT rInc, rDec, rDrag;
	SpinBox_GetRects( pSBI->hWnd, &rInc, &rDec, &rDrag );

	RECT rDragOriginal = rDrag;

	WCHAR cDrag = 0xF0F3;

	/*TCHAR cDec = 0xF0E9;
	TCHAR cInc = cDec+1;
	if( SpinBox_GetLayout( pSBI->hWnd ) == SBLAYOUT_VERTICAL )
	{
		cDec = 0xF0EC;
		cInc = 0xF0EB;
	}*/

	UINT dtFormat = DT_SINGLELINE | DT_VCENTER | DT_CENTER | DT_NOCLIP;

	// This raises the arrow in the hot part of the button.
	/*if( !pSBI->bDragging )
	{
		if( pSBI->hotPart == SBP_DRAG )
			OffsetRect( &rDrag, -1, -1 );
		else if( pSBI->hotPart == SBP_INCREMENT )
			OffsetRect( &rInc, -1, -1 );
		else if( pSBI->hotPart == SBP_DECREMENT )
			OffsetRect( &rDec, -1, -1 );
	}*/

	if( !IsRectEmpty( &rDrag ) )
		DrawTextW( hDC, &cDrag, 1, &rDrag, DT_SINGLELINE | DT_VCENTER | DT_CENTER );


	/*if( !IsRectEmpty( &rDec ) )
	{
		if( pSBI->layout == SBLAYOUT_VERTICAL )
			OffsetRect( &rDec, 1, 3 );
		DrawText( hDC, &cDec, 1, &rDec, dtFormat );
	}


	if( !IsRectEmpty( &rInc ) )
	{
		if( pSBI->layout == SBLAYOUT_VERTICAL )
			OffsetRect( &rInc, 0, -6 );
		DrawText( hDC, &cInc, 1, &rInc, dtFormat );
	}*/

	SetTextColor( hDC, oldColor );
	SetBkMode( hDC, oldBkMode );
	SelectObject( hDC, hOldFont );

	// Restore rDrag for proper edge drawing.
	rDrag = rDragOriginal;

	if( !SpinBox_IsDragOnly( pSBI->hWnd ) )
	{
		// Draw the edge thingy separating the spin box parts.
		INT edgeLength = 12;
		if( SpinBox_GetLayout( pSBI->hWnd ) == SBLAYOUT_HORIZONTAL )
		{
			//edgeLength = MIN( edgeLength, RECTHEIGHT(rDrag) - 6 );
			edgeLength = MAX( edgeLength, RECTHEIGHT(rDrag)-8 );
			rDrag.top = RECTHEIGHT(rDrag)/2 - edgeLength/2;
			rDrag.bottom = rDrag.top + edgeLength;
			DrawEdge( hDC, &rDrag, EDGE_ETCHED, BF_LEFT );
			DrawEdge( hDC, &rDrag, EDGE_ETCHED, BF_RIGHT );
		}
		else
		{
			//edgeLength = MIN( edgeLength, RECTWIDTH(rDrag) - 4 );
			edgeLength = MAX( edgeLength, RECTWIDTH(rDrag)-8 );
			rDrag.left = RECTWIDTH(rDrag)/2 - edgeLength/2;
			rDrag.right = rDrag.left + edgeLength;
			DrawEdge( hDC, &rDrag, EDGE_ETCHED, BF_TOP );
			DrawEdge( hDC, &rDrag, EDGE_ETCHED, BF_BOTTOM );
		}
	}
}





// The window procedure.
LRESULT CALLBACK A3DSpinBoxWndProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
{
	SpinboxInfo* pSBI = GetControlData( hWnd );

	switch( Msg )
	{
	case WM_NCCREATE:
		if( NULL == (pSBI = new SpinboxInfo( hWnd ) ) )
			return FALSE;
		SetControlData( hWnd, pSBI );
		pSBI->hNotifyWnd = GetParent( hWnd );
		return TRUE;


	case WM_NCDESTROY:
		delete pSBI;
		break;


	case WM_CREATE:
		if( !g_hGlyphFont )
		{
			// Create the font used to draw the arrows.
			LOGFONT lf;
			ZeroMemory( &lf, sizeof(LOGFONT) );
			//GetObject( GetStockObject( DEFAULT_GUI_FONT ), sizeof(LOGFONT), &lf );
			StringCchCopy( lf.lfFaceName, ARRAY_SIZE( lf.lfFaceName ), TEXT( "Wingdings" ) );
			lf.lfCharSet = SYMBOL_CHARSET;
			//lf.lfHeight = 22;
			lf.lfQuality = ANTIALIASED_QUALITY;
			//lf.lfQuality = CLEARTYPE_QUALITY;
			
			g_hGlyphFont = CreateFontIndirect( &lf );
			if( !g_hGlyphFont )
			{
				OutputDebugString( TEXT( __FUNCTION__ )TEXT(": Failed to load Wingdings font" ) );
				return -1;
			}

			lf.lfWeight = FW_BLACK;
			//lf.lfHeight += 8;
			g_hBigGlyphFont = CreateFontIndirect( &lf );
		}

		g_uxTheme.Load();
		if( g_uxTheme.IsThemeActive() )
			pSBI->hButtonTheme = g_uxTheme.OpenThemeData( hWnd, L"Button" );

		SpinBox_CalculateLayout( hWnd );
		break;


	case WM_SIZE:
		InvalidateRect( hWnd, NULL, TRUE );
		SpinBox_CalculateLayout( hWnd );
		//UpdateWindow( hWnd );
		break;


	case WM_THEMECHANGED:
		if( pSBI->hButtonTheme && g_uxTheme.Load() )
		{
			g_uxTheme.CloseThemeData( pSBI->hButtonTheme );
			pSBI->hButtonTheme = NULL;
			if( g_uxTheme.IsThemeActive() )
				pSBI->hButtonTheme = g_uxTheme.OpenThemeData( hWnd, L"Button" );
		}
		break;




	case WM_LBUTTONDOWN:
		{
			// Notify parent that it has been clicked. Proceed with normal processing unless parent returns nonzero.
			NMSPINBOX nm = {0};
			nm.hdr.hwndFrom = hWnd;
			nm.hdr.idFrom = GetWindowLong( hWnd, GWL_ID );
			nm.hdr.code = NM_LDOWN;
			if( 0 != SendMessage( pSBI->hNotifyWnd, WM_NOTIFY, (WPARAM)nm.hdr.idFrom, (LPARAM)&nm ) )
				return 0;

			g_hDragSpinBox = hWnd;
			g_nDragDir = 0;
			GetCursorPos( &g_ptMouseStart );
			SetCapture( hWnd );
			g_hOldCursor = SetCursor( LoadCursor( NULL, IDC_SIZEALL ) );
			// Redraw the window to update the visual state (it's now pressed).
			InvalidateRect( hWnd, NULL, TRUE );
			UpdateWindow( hWnd );
		}
		return 0;


	case WM_LBUTTONUP:
		if( GetCapture() == hWnd )
			ReleaseCapture();
		break;



	case WM_MOUSEMOVE:
		{
			// This is to have the window receive WM_MOUSELEAVE and WM_MOUSEHOVER, which are cool!
			TRACKMOUSEEVENT tme = {0};
			tme.cbSize = sizeof(TRACKMOUSEEVENT);
			tme.hwndTrack = hWnd;

			tme.dwHoverTime = HOVER_DEFAULT;
			tme.dwFlags = TME_LEAVE;// | TME_HOVER;
			TrackMouseEvent( &tme );
		}
		{
			POINT ptCursor, ptCursorClient;
			GetCursorPos( &ptCursor );
			ptCursorClient = ptCursor;
			ScreenToClient( hWnd, &ptCursorClient );
			BOOL bDragStart = FALSE;

			if( g_hDragSpinBox == hWnd )
			{
				// Mouse is being tracked.
				if( g_nDragDir == 0 )
				{
					// Establish drag direction.
					if( abs( ptCursor.x - g_ptMouseStart.x ) >= GetSystemMetrics( SM_CXDRAG ) )
					{
						g_nDragDir = 1;
						SetCursor( LoadCursor( NULL, IDC_SIZEWE ) );
					}
					else if( abs( ptCursor.y - g_ptMouseStart.y ) >= GetSystemMetrics( SM_CYDRAG ) )
					{
						g_nDragDir = 2;
						SetCursor( LoadCursor( NULL, IDC_SIZENS ) );
					}
					if( g_nDragDir != 0 )
						bDragStart = TRUE;
				}
				if( g_nDragDir != 0 )
				{
					INT dx = ptCursor.x - g_ptPrevCursor.x;
					INT dy = ptCursor.y - g_ptPrevCursor.y;
					//if( pSBI->bVertical )	{ if( abs(dy) < (int)pSBI->dragPixels ) break; }
					//else					{ if( abs(dx) < (int)pSBI->dragPixels ) break; }
					INT delta;
					if( g_nDragDir == 2 )	delta = -dy / (int)pSBI->dragPixels;
					else					delta = dx / (int)pSBI->dragPixels;

					if( delta == 0 ) break;

					NMSPINBOX nm = {0};
					nm.hdr.hwndFrom = hWnd;
					nm.hdr.idFrom = GetWindowLong( hWnd, GWL_ID );
					nm.wParam = wParam;
					nm.deltaValue = delta;//pSBI->bVertical ? -dy : dx;

					if( bDragStart )
					{
						nm.hdr.code = SBN_DRAGSTART;
						nm.additionalInfo = SB_FIRST_CHANGE;
						SendMessage( pSBI->hNotifyWnd, WM_NOTIFY, (WPARAM)nm.hdr.idFrom, (LPARAM)&nm );
					}
					nm.hdr.code = SBN_DELTAVALUE;
					SendMessage( pSBI->hNotifyWnd, WM_NOTIFY, (WPARAM)nm.hdr.idFrom, (LPARAM)&nm );
				}
				g_ptPrevCursor = ptCursor;
			}
			else
			{
				// Not dragging, do hot tracking.
				INT hotPart = SpinBox_DoHitTest( hWnd, ptCursorClient );
				if( hotPart != pSBI->hotPart || !pSBI->bHot )
				{
					pSBI->hotPart = hotPart;
					pSBI->bHot = TRUE;
					InvalidateRect( hWnd, NULL, TRUE );
					UpdateWindow( hWnd );
				}
			}
		}
		break;


	case WM_MOUSELEAVE:
		pSBI->bHot = FALSE;
		pSBI->hotPart = SBP_NONE;
		InvalidateRect( hWnd, NULL, TRUE );
		UpdateWindow( hWnd );
		break;




	case WM_CAPTURECHANGED:
		if( hWnd != (HWND)lParam )
			SpinBox_EndDragging( hWnd, TRUE );
		break;


	case WM_ENABLE:
		if( GetCapture() == hWnd )
			ReleaseCapture();
		InvalidateRect( hWnd, NULL, TRUE );
		UpdateWindow( hWnd );
		break;


	case WM_ERASEBKGND:
		{
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			if( pSBI->hButtonTheme )
				DrawSpinBoxBGThemed( pSBI, &rClient, NULL, (HDC)wParam );
			else
				DrawSpinBoxBG( pSBI, &rClient, (HDC)wParam );
		}
		return 1;


	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdcPaint = BeginPaint( hWnd, &ps );
			if( !hdcPaint ) { EndPaint( hWnd, &ps ); break; }
			// Paint the glyphs.
			RECT rClient;
			GetClientRect( hWnd, &rClient );
			DrawSpinBox( pSBI, &rClient, hdcPaint );
			EndPaint( hWnd, &ps );
		}
		return 0;
	}

	return DefWindowProc( hWnd, Msg, wParam, lParam );
}



BOOL A3DSpinBoxInited = FALSE;
ATOM A3DSpinBoxAtom = 0;

BOOL A3DSpinBox_Init( HINSTANCE hInstance )
{
	if( A3DSpinBoxInited )
		return A3DSpinBoxAtom != NULL;
	A3DSpinBoxInited = TRUE;

	WNDCLASSEX wcex;
	ZeroMemory( &wcex, sizeof(WNDCLASSEX) );

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_GLOBALCLASS;
	wcex.lpfnWndProc = A3DSpinBoxWndProc;
	wcex.cbWndExtra = sizeof( SpinboxInfo* );
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
	wcex.hbrBackground = (HBRUSH)(COLOR_3DFACE+1);
	wcex.lpszClassName = WC_A3DSPINBOX;

	A3DSpinBoxAtom = RegisterClassEx( &wcex );
	if( NULL == A3DSpinBoxAtom )
		return FALSE;

	// Glyph font and theme data are created when the control is created.

	return TRUE;
}





VOID A3DSpinBox_Uninit()
{
	if( !A3DSpinBoxInited )
		return;
	A3DSpinBoxInited = FALSE;
	if( A3DSpinBoxAtom != NULL )
	{
		UnregisterClass( (LPCTSTR)A3DSpinBoxAtom, GetModuleHandle(0) );
		A3DSpinBoxAtom = NULL;
	}
	if( g_hGlyphFont )
	{
		DeleteObject( g_hGlyphFont );
		g_hGlyphFont = NULL;
	}
	if( g_hBigGlyphFont )
	{
		DeleteObject( g_hBigGlyphFont );
		g_hBigGlyphFont = NULL;
	}
}