/// @file Utility funcions and macros
/// -Adel Amro (http://code-section.com)


# include <stdlib.h>
# include <math.h>


# ifndef CSTB_UTIL_H
# define CSTB_UTIL_H


#if defined( _DEBUG ) && !defined( DEBUG )
#define DEBUG
#endif


#ifndef PI
#define PI 3.14159265358979f
#endif

#ifndef DEGTORAD
/// Macro to convert from degrees to radians.
#define DEGTORAD(d)				( (d)*PI/180 )
#endif

#ifndef RADTODEG
/// Macro to convert from radians to degrees.
#define RADTODEG(r)				( (r)*180/PI )
#endif

#ifndef SAFE_DELETE
/// For pointers allocated with new.
#define SAFE_DELETE(p)			{ if(p) { delete (p);     (p)=NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
/// For arrays allocated with new [].
#define SAFE_DELETE_ARRAY(p)	{ if(p) { delete[] (p);   (p)=NULL; } }
#endif

#ifndef SAFE_RELEASE
/// For use with COM pointers.
#define SAFE_RELEASE(p)			{ if(p) { (p)->Release(); (p)=NULL; } }
#endif

template<class T>
inline int SafeRelease( T*& p ) { int ret = 0; if( p ) { ret = p->Release(); p = 0; } return ret; }

/// TODO: Add SAFE_DELETE_OBJECT() for GDI objects.
#define SAFE_DESTROY_WINDOW(hWnd)	{ if(hWnd) { DestroyWindow(hWnd); hWnd = NULL; } }

#ifndef ARRAY_SIZE
/// Returns the number of elements in a stack-allocated array.
#define ARRAY_SIZE(x)			( (sizeof((x))/sizeof((x)[0])) )
#endif

#ifndef SET_ARRAY
#define SET_ARRAY( a, val )		for( unsigned i=0; i<sizeof(a)/sizeof(a[0]); i++ ) a[i] = val;
#endif

#ifndef CLEAR_ARRAY
#define CLEAR_ARRAY( a ) SET_ARRAY( a, 0 )
#endif

#ifndef DLG_RETURL
#define DLG_RETURN(x) {SetWindowLong(hDlg,DWL_MSGRESULT,(x));return (x);}
#endif

#ifndef MIN
#define MIN(x,y)				( (x)<(y)? (x) : (y) )
#endif

#ifndef MAX
#define MAX(x,y)				( (x)>(y)? (x) : (y) )
#endif

#ifndef CLAMP
#define CLAMP(x,a,b)			( (x)>(b) ? (b) : (x)<(a) ? (a) : (x) )
#endif

#ifndef CLAMP01
#define CLAMP01(x)				( (x)>1 ? 1 : (x) < 0 ? 0 : (x) )
#endif

#ifndef RECTWIDTH
#define RECTWIDTH(r)			( ((r).right - (r).left) ) ///< The height of a RECT structure.
#endif

#ifndef RECTHEIGHT
#define RECTHEIGHT(r)			( ((r).bottom - (r).top) ) ///< The width of a RECT structure.
#endif

#ifndef RECTCENTERX
#define RECTCENTERX(r)			( ((r).left + RECTWIDTH(r)/2) ) ///< The x coordinate of the center of a RECT structure.
#endif

#ifndef RECTCENTERY
#define RECTCENTERY(r)			( ((r).top + RECTHEIGHT(r)/2) ) ///< The y coordinate of the center of a RECT structure.
#endif

#ifndef ABS
#define ABS(x)					( (x)<0?-(x):(x) )
#endif
//#ifndef ROUND
//#define ROUND(x)				( ((x) - (INT)(x) < 0.5f ? (INT)(x) : (INT)(x) + 1) )
//#endif
#ifndef ISKEYDOWN
//#define ISKEYDOWN(x)			( (GetAsyncKeyState((x))&0x8000)!=0 ) ///< Returns whether a key or mouse button is pressed.
#define ISKEYDOWN(x)			( (GetKeyState((x))&0x8000)!=0 ) ///< Returns whether a key or mouse button is pressed.
#endif

#ifndef LINEAR_INTERPOLATE
//#define LINEAR_INTERPOLATE(a,b,t) ( (a)*(1-(t)) + (b)*(t) )
#define LINEAR_INTERPOLATE(a,b,t)	( (a)+(t)*((b)-(a)) )
#endif

#ifndef LERP
#define LERP LINEAR_INTERPOLATE
#endif

template <class T, class U>
inline T LinearInterpolate(T a, T b, U u) { return a + u * (b - a); }

template <class T>
inline T CosineInterpolate( T a, T b, T t ) {
	t = (1-cos(t*PI))/2;
	return(a*(1-t)+b*t);
}

template<class T>
inline T SmoothStep( T t ) { return t * t * (3 - 2*t); }


template<class V>
V BezierCurve( V pts[4], float t )
{
	float it = 1.f - t;
	return (it*it*it) * pts[0] + 3 * (it*it) * t * pts[1] + 3 * it * (t*t) * pts[2] + t*t*t * pts[3];
}


/// Increments an integer when constructed, decrements it when destroyed.
//struct IncDec
//{
//	int& n;
//	IncDec(int& _n) : n(_n) { n++; }
//	~IncDec() { n--; }
//};



# ifndef SWAP
/// Swap template function.
template <class T>
inline void SWAP( T& t1, T& t2 ) { T temp = t1; t1 = t2; t2 = temp; }
# endif


inline int ceiln(double d) { return (int)(d + 0.5 - (d < 0)); }


/// Memory comparison. Returns 0 if memory is identical, 1 if p1 is greater (bytewise), -1 if it's smaller. 'length' is in bytes.
inline int CompareMemory( const void* p1, const void* p2, unsigned length )
{
	char* b1 = (char*)p1, *b2 = (char*)p2;
	for( unsigned i=0; i<length; i++ )
	{
		if( b1[i] > b2[i] )
			return 1;
		if( b1[i] < b2[i] )
			return -1;
	}
	return 0;
}



/// floating point rounding.
inline long fround( float f ) { if( f < 0 ) return (long)(f - 0.5f); return (long)(f + 0.5f); }

/// Returns a random positive float value ranging from 0 to 1.
inline float frand01() { return (float)rand()/RAND_MAX; }

/// Returns a random float value ranging from fMin to fMax.
inline float frand( float fMin, float fMax ) { return fMin + (fMax - fMin) * ((float)rand()/RAND_MAX); }

/// Returns a random positive float value.
inline float frand() { return (float)rand()/RAND_MAX + rand(); }

//inline INT Align( INT n, INT boundary ) { INT rem = n % boundary; if( rem ) n += (boundary-rem); return n; }
/// Used for memory alignment. Returns the result of rounding 'n' to the smallest multiple of 'boundary'.
inline unsigned Align( unsigned n, unsigned boundary )
{ if( !boundary ) return n; return ((n + boundary - 1) / boundary) * boundary; }



inline float PointDistanceSq( float ax, float ay, float bx, float by )
{ float dx = ax - bx; float dy = ay - by; return (dx * dx + dy * dy ); }

template <class V2>
inline float PointDistanceSq(V2 a, V2 b)
{ float dx = a.x - b.x; float dy = a.y - b.y; return(dx * dx + dy * dy); }


inline float PointDistance( float ax, float ay, float bx, float by )
{ return sqrtf( PointDistanceSq( ax, ay, bx, by ) ); }

template <class V2>
inline float PointDistance( V2 a, V2 b )
{ return sqrtf(PointDistanceSq(a, b)); }


/// Note: Code for this function is from a Philip Nicoletti on the CodeGuro forums
/// http://www.codeguru.com/forum/showthread.php?t=194400
///
/// Find the distance from the point (cx,cy) to the line
/// determined by the points (ax,ay) and (bx,by)
///
/// distanceSegment = distance from the point to the line segment
/// distanceLine = distance from the point to the line (assuming
///					infinite extent in both directions
///
void DistanceFromLine( float cx, float cy, float ax, float ay,
	float bx, float by, float& distanceSegment,
	float& distanceLine );

template <class V2>
inline void DistanceFromLine(V2 a, V2 b, V2 pt, float& segmentDist, float& lineDist)
{ DistanceFromLine(pt.x, pt.y, a.x, a.y, b.x, b.y, segmentDist, lineDist ); }


template <class V2>
inline float ProjectPointOnLine(V2 a, V2 b, V2 pt)
{
	float r_numerator = (pt.x - a.x) * (b.x - a.x) + (pt.y - a.y) * (b.y - a.y);
	float r_denomenator = (b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y);
	return r_numerator / r_denomenator;
}


// 2D vector rotation
template <class V2>
inline V2 RotateVector(V2 v, float r)
{
	float cs = cosf(r);
	float sn = sinf(r);
	return V2( v.x * cs - v.y * sn, v.x * sn + v.y * cs );
}




bool StringStartsWith( const char* sText, int textLen, const char* sPrefix, int prefixLen );
bool StringStartsWith( const wchar_t* sText, int textLen, const wchar_t* sPrefix, int prefixLen );

int FindSubstring( const char* sText, int textLen, const char* sFind, int findLen );
int FindSubstring( const wchar_t* sText, int textLen, const wchar_t* sFind, int findLen );

/// Copies one string to another. The function will 0-terminate the destination buffer,
/// unless its size is 0. It returns the number of characters copied to the destination
/// buffer, not including the terminating 0.
/// The length of the source string can be -1, but the function will stop copying when it
/// encounters a 0 in the source string.
int StrncpyEx( char* dest, int destSize, const char* src, int srcLen );

/// String replacement. Returns the number of characters written to the destination buffer, not including
/// the terminating 0.
/// maxOccurrances can optionally be used to prevent the function from replacing all occurrances
/// of the substring to find.
int ReplaceString( char* dest, int destSize, const char* src, const char* find, const char* replace,
	int maxOccurrances = -1 );


# define CSTB11
# ifdef CSTB11
# include <functional>
namespace CSTB
{
	template <typename Function>
	struct function_traits : public function_traits<decltype(&Function::operator())>
	{};

	template <typename ClassType, typename ReturnType, typename... Args>
	struct function_traits<ReturnType(ClassType::*)(Args...) const>
	{
		typedef ReturnType(*pointer)(Args...);
		typedef std::function<ReturnType(Args...)> function;
	};

	template <typename Function>
	typename function_traits<Function>::pointer
	fptr(Function& lambda) { return static_cast<typename function_traits<Function>::pointer>(lambda); }
}

# endif



# endif // inclusion guard.